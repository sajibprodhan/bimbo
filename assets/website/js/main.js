// preloader

'use strict';

jQuery(".pa-ellipsis").fadeOut(), jQuery(".pa-preloader").delay(300).fadeOut("300");

// Scroll To Top Button
$(window).scroll(function(){
  if ($(this).scrollTop() > 200) {
    $('#scrollTopBtn').fadeIn();
  } else {
    $('#scrollTopBtn').fadeOut();
  }
});
$('#scrollTopBtn').click(function(){
  $("html, body").scrollTop(0);
  //$("html, body").animate({ scrollTop: 0 }, 600);
  return false;
});

// Fixed Header
var $scroll = $(this).scrollTop();
if($scroll>=100){
	$('body').addClass('fixed-header');
}else{
	$('body').removeClass('fixed-header');
}
$(window).scroll(function(){
	$scroll = $(this).scrollTop();
	if($scroll>=100){
		$('body').addClass('fixed-header');
	}else{
		$('body').removeClass('fixed-header');
	}
});


// Data Delete Btn Sweet Alert (Delete Method)
$('.data-delete-btn').on('click', function(e){
    e.preventDefault();
    swal({
        title: "Careful!",
        text: "Are you sure?",
        icon: "warning",
        dangerMode: true,
        buttons: {
          cancel: "Exit",
          confirm: "Confirm",
        },
    })
    .then ((willDelete) => {
        if (willDelete) {
           $(this).closest("form").submit();
        }
    });
});

// Data Delete Btn Sweet Alert (Get Method)
$('.data-delete-btn-label').on('click', function(e){
    e.preventDefault();
    swal({
        title: "Careful!",
        text: "Are you sure?",
        icon: "warning",
        dangerMode: true,
        buttons: {
          cancel: "Cancel",
          confirm: "Confirm",
        },
    })
    .then ((willDelete) => {
        if (willDelete) {
           $(this).parent('.cart-item-remove-btn')[0].click();
        }
    });
});

// Navbar Toggle Button For Mini Device
$('.navbar-menu-toggle-btn').click(function(){
	$('.navbar-main').toggleClass('menu-visible');
	$('body').toggleClass('body-overflow');
});


	// Navbar Submenu Make Collapse In Mini Device
	$(window).on('load', function() {
	if($(window).width() <= 1024){
		$('.nav-item-submenu').addClass('collapse');
	}else{
		$('.nav-item-submenu').removeClass('collapse');
	}
});
$(window).on('resize', function() {
	if($(window).width() <= 1024){
		$('.nav-item-submenu').addClass('collapse');
	}else{
		$('.nav-item-submenu').removeClass('collapse');
	}
	if($(window).width() >= 1024){
	    $('body').removeClass('body-overflow');
	  }
});

// Navbar Toggle Button For Mini Device
$(".item-has-submenu .nav-item-link").click(function(e){
    if ($(window).width() <= 1024) {
        e.preventDefault();
        $(this).parent().find(".collapse").collapse('toggle');
    }
});


// Home Main Top Slider
$('.home-main-slider-inner').owlCarousel({
    loop:true,
    margin:0,
    mouseDrag:true,
    autoplay:true,
    // animateOut: 'slideOutDown',
  	// animateIn: 'flipInX',
  	smartSpeed:1000,
  	autoplayTimeout:6000,
    autoplayHoverPause: false,
    responsiveClass: true,
    navText: ['<span><i class="fas fa-angle-left"></i></span>','<span><i class="fas fa-angle-right"></i></span>'],
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    },
    dots: false,
    nav:true,
});


// Home Photo Gallery Slider
$('.photo-video-gallery-outer.photo .slider-for').slick({
  autoplay: true,
  autoplaySpeed: 4000,
  infinite: true,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: true,
  fade: true,
  asNavFor: '.photo .slider-nav'
});
$('.photo-video-gallery-outer.photo .slider-nav').slick({
  slidesToShow: 4,
  slidesToScroll: 1,
  asNavFor: '.photo .slider-for',
  arrows: true,
  centerMode: true,
  focusOnSelect: true
});

// Home Video Gallery Slider
$('.photo-video-gallery-outer.video .slider-for').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
  asNavFor: '.video .slider-nav'
});
$('.photo-video-gallery-outer.video .slider-nav').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  asNavFor: '.video .slider-for',
  arrows: true,
  centerMode: true,
  focusOnSelect: true
});


// Advertisement Slider
$('.home-main-news-slider-inner').owlCarousel({
    loop:true,
    margin:0,
    mouseDrag:false,
    autoplay:true,
    // animateOut: 'slideOutDown',
    // animateIn: 'flipInX',
    smartSpeed:1000,
    autoplayTimeout:10000,
    autoplayHoverPause: false,
    responsiveClass: true,
    //navText: ['<span><i class="fas fa-angle-left"></i></span>','<span><i class="fas fa-angle-right"></i></span>'],
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    },
    nav: false,
    dots: false,
    dotsData: true,
    //nav:true,
});

// Advertisement Slider
$('.a-banner-slider-inner').owlCarousel({
    loop:true,
    margin:0,
    mouseDrag:false,
    autoplay:true,
    // animateOut: 'slideOutDown',
    // animateIn: 'flipInX',
    smartSpeed:1000,
    autoplayTimeout:6000,
    autoplayHoverPause: false,
    responsiveClass: true,
    //navText: ['<span><i class="fas fa-angle-left"></i></span>','<span><i class="fas fa-angle-right"></i></span>'],
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    },
    dots: true,
    dotsData: true,
    //nav:true,
});

// Image Gallery
if ( $(".image-grid-inner").length>0 ){
  $('body').imagesLoaded( function() {
    $('.image-grid-inner').GridHorizontal({
      item: '.item',
      minWidth: 400,
      maxRowHeight: 350,
      gutter: 5,
    });
  });
  (function() {
      var $gallery = new SimpleLightbox('.image-grid-inner a', {});
  })();
}


// Headline Marquee JS
setTimeout(function(){
  jQuery('.main-headline-section .marquees').css("opacity","1");

}, 100);
jQuery('.main-headline-section .marquees').marquee({
  speed: 35000,
  gap: 0,
  delayBeforeStart: 0,
  direction: 'left',
  duplicated: true,
  pauseOnHover: true
});
// JS for Progress Bar
$('.progressbar').progressBar({
    shadow: true,
    percentage: true,
    animation: true,
    barColor: "#00A99D",
});


// Image Gallery Preloader
$(window).on('load', function() { // makes sure the whole site is loaded
  $('#status').fadeOut(1000); // will first fade out the loading animation
  $('#preloader').delay(350).fadeOut(1000); // will fade out the white DIV that covers the website.
  setTimeout(function(){
    $('.image-gallery-grid-section .image-grid-inner').delay(350).css({'opacity':'1'});
  },1000);
})

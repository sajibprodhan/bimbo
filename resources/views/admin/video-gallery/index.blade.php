@extends('layouts.admin')

@section('content')
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div class="page-title-icon"> <i class="pe-7s-display1 icon-gradient bg-premium-dark"></i>
                    </div>
                    <div>Video Gallery Video List
                    </div>
                    <button type="button" class="btn btn-primary float-right add-new-modal-btn">Add New</button>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="main-card mb-3 card">
                    <div class="card-body">
                        {{-- <h5 class="card-title">Simple table</h5> --}}
                        <table id="table_id" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>SL</th>
                                    <th>Video</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($video_gallery_data->count() > 0)
                                    @foreach ($video_gallery_data as $video)
                                    <tr>
                                        <td>{{ $loop->index+1 }}</td>
                                        <td class="all-video-iframe">
                                            {!! $video->video_url !!}
                                        </td>
                                        <td>
                                            <div class="action-btns">
                                               {{--  <a class="data-edit-btn" data-toggle="tooltip" data-placement="top" title="Edit" href="{{ route('admin.post-category.edit', $category->id) }}">
                                                <i class="fas fa-edit"></i>
                                                </a> --}}
                                                <form method="POST" action="{{ route('admin.video-gallery.destroy', $video->id) }}" accept-charset="UTF-8">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="data-delete-btn" data-toggle="tooltip" data-placement="top" title="Delete Category"><i class="fas fa-trash"></i></button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                @else
                                <h3 style="text-alig:center"> No Post Found</h3>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Add New Category Form Modal -->
    <div class="add-new-modal-wrapper">
        <div class="add-new-modal-inner">
            <div class="main-card card">
                <div class="card-body">
                    <div class="card-top-wrapper">
                        <h5 class="card-title">Upload New Video(s)</h5>
                        <div class="modal-hide-btn"><i class="fas fa-times"></i></div>
                    </div>
                    <form action="{{ route('admin.video-gallery.store') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="position-relative form-group border-0">
                            <label class="mb-0">Video Embeded Link</label>
                            <div class="avatar-upload top-photo">
                                <div class="product-video-items-wrapper">
                                    <div class="product-cover-photo">
                                        <input class="form-control p-cover-photo" type="text"  name="video_url[]" required>
                                        <div class="p-video-new-item-add-btn"><i class="fas fa-plus"></i>Add New</div>
                                    </div>
                                </div>                                
                            </div>
                        </div>
                        <button class="mt-1 btn btn-primary p-store-btn" type="submit">Submit</button>
                        <button class="mt-1 btn modal-hide-btn cancel" type="reset">Cancel</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

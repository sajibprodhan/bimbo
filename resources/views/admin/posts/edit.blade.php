@extends('layouts.admin')
@section('content')
<div class="app-main__inner">
	<div class="app-page-title">
		<div class="page-title-wrapper">
			<div class="page-title-heading">
				<div class="page-title-icon"> <i class="pe-7s-display1 icon-gradient bg-premium-dark"></i>
				</div>
				<div>Edit Post Page
				</div>
			</div>
		</div>
	</div>
	<div class="tab-content">
		<div class="tab-pane tabs-animation fade show active" id="tab-content-0" role="tabpanel">
			<div class="row">
				<div class="col-md-8 m-auto">
					<div class="main-card mb-3 card">
						<div class="card-body">
							<h5 class="card-title">Edit Post</h5>
							<form class="" action="{{ route('admin.post.update', $post_data->id) }}" method="POST" enctype="multipart/form-data">
								@csrf
								@method('PUT')
								<div class="position-relative form-group">
									<label for="exampleEmail" class="">Post Title</label>
									<input name="title" id="exampleEmail" placeholder="Enter post name" type="text" class="form-control" required value="{{$post_data->title}}">
								</div>
								<div class="position-relative form-group">
									<label for="commonSelect" class="">Select Category</label>
									<?php
										$post_category = DB::table('post_categories')->get();
									?>
									@if(count($post_category) > 0)
										<select class="form-control multiCatSelect" name="post_category[]" id="commonSelect" multiple required>
											@foreach($post_data->post_category_id as $category_id)
                                                <?php
                                                $categoryData = DB::table('post_categories')->where('id',$category_id)->first();
                                                ?>
                                                @if($categoryData)
                                                <option selected value="{{$categoryData->id}}">{{$categoryData->category_title}}</option>
                                                @endif
                                            @endforeach
											@foreach($post_category as $pCategory)
											<option value="{{$pCategory->id}}">{{ $pCategory->category_title }}</option>
											@endforeach
										</select>
									@endif
								</div>
                                <div class="position-relative form-group">
									<label for="exampleEmail" class="">Description</label>
                                    <textarea id="HCKEditor" name="description" id="exampleText" class="form-control" cols="30" rows="5" required>{{ $post_data->description }}</textarea>
								</div>

								<div class="position-relative form-group">
									<label for="exampleSelect" class="">Status</label>
									<select name="status" id="exampleSelect" class="form-control" required>
										<option {{($post_data->status ==0)?'selected':''}} value="0">Unpublish</option>
										<option {{($post_data->status ==1)?'selected':''}} selected value="1">Publish</option>
									</select>
								</div>

								<div class="position-relative form-group border-0">
					                <label class="mb-0">Cover Photo</label>
					                <div class="avatar-upload top-photo d-flex">
					                    <div class="avatar-edit">
					                        <?php
					                            $productImageData =  DB::table('post_images')->where('post_id', $post_data['id'])->get();
					                        ?>
					                    </div>
					                    @if(count($productImageData) > 0)
					                        @foreach($productImageData as $productImage)
					                            <div class="single-product-image-prev-item" data-id="{{ $productImage->id }}">
					                                <span class="sp-image-remove">
					                                    <i class="fas fa-times-circle"></i>
					                                </span>
					                                <div class="avatar-image-preview" id="productCoverPrev" style="background-image: url({{ asset($productImage->post_image) }});"></div>
					                                @if($productImage->default_image == 1)
					                                <span class="sp-default-img">Cover</span>
					                                @endif
					                            </div>
					                        @endforeach
					                    @endif
					                </div>

					                <div class="avatar-upload top-photo">
					                    <small class="form-text text-muted mt-0">File Size Must Be Less Than 1 MB</small>
					                    <div class="product-cover-photo-items-wrapper">
					                        <div class="product-cover-photo">
					                            <input class="p-cover-photo" type="file" accept="image/*" name="post_image[]">
					                            <input type="hidden" name="default_image[]" value="1">
					                            {{-- <div class="check-wrapper">
					                                <input class="product-cover-default-checkbox" type="checkbox" name="default_image[]" checked>
					                                <div class="box"></div>
					                                <label>Make As Default</label>
					                            </div>
					                            <div class="p-image-new-item-add-btn"><i class="fas fa-plus"></i>Add New</div>
					                        </div> --}}
					                    </div>
					                </div>
					            </div>

								<button class="mt-1 btn btn-primary p-store-btn">Submit</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).on('click','.sp-image-remove', function(e){
	    var thisParent = $(this).closest('.single-product-image-prev-item');
	    var thisId = thisParent.attr('data-id');
	    $.ajax({
	        url:'/admin/post/image/delete/'+thisId,
	        type:'get',
	        success: function (data){
	        	console.log(data);
	            if(data.success == 1){
	                thisParent.remove();
	                if(data.is_default == 1){
	                    $('.single-product-image-prev-item').last().append('<span class="sp-default-img">Cover</span>');
	                }
	            }
	        }
	    });
	});
</script>
@stop

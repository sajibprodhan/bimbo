@extends('layouts.admin')
@section('content')
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div class="page-title-icon"> <i class="pe-7s-display1 icon-gradient bg-premium-dark"></i>
                    </div>
                    <div>Post list
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="main-card mb-3 card">
                    <div class="card-body">
                        <table id="table_id" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>SL</th>
                                    <th>Date</th>
                                    <th>Post Title</th>
                                    <th>Post Category</th>
                                    <th>Image</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($posts->count() > 0)
                                    @foreach ($posts as $post)
                                    <tr>
                                        <td>{{ $loop->index+1 }}</td>
                                        <td>
                                            <div class="date-time">{{  date('M d, Y', strtotime($post->created_at)) }}</div>
                                        </td>
                                        <td>{{ $post->title }}</td>
                                        <td class="post-category">
                                            @foreach($post->post_category_id as $category_id)
                                                <?php
                                                $categoryData = DB::table('post_categories')->where('id',$category_id)->first();
                                                ?>
                                                @if($categoryData)
                                                <span>{{$categoryData->category_title}} ,</span>
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>
                                            <?php
                                            $imageData = DB::table('post_images')->where('post_id',$post->id)->where('default_image', 1)->first();
                                            ?>
                                            @if($imageData)
                                                <a href="{{ asset($imageData->post_image) }}">
                                                <img src="{{ asset($imageData->post_image) }}" width="82" /></a>
                                            @endif
                                        </td>
                                        <td>
                                            {!! ($post->status) ? '<a class="badge badge-info">Publish </a>': '<a class=" badge badge-warning">Unpublish</a>' !!}
                                        <td>
                                            <div class="action-btns">
                                                <a class="data-edit-btn" data-toggle="tooltip" data-placement="top" title="Edit" href="{{ route('admin.post.edit', $post->id) }}">
                                                <i class="fas fa-edit"></i>
                                                </a>
                                                <form method="POST" action="{{ route('admin.post.destroy', $post->id) }}" accept-charset="UTF-8">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="data-delete-btn" data-toggle="tooltip" data-placement="top" title="Delete Category"><i class="fas fa-trash"></i></button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                @else
                                <h3 style="text-alig:center"> No Post Found</h3>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@extends('layouts.admin')
@section('content')
<div class="app-main__inner">
	<div class="app-page-title">
		<div class="page-title-wrapper">
			<div class="page-title-heading">
				<div class="page-title-icon"> <i class="pe-7s-display1 icon-gradient bg-premium-dark"></i>
				</div>
				<div>Add Post Page
				</div>
			</div>
		</div>
	</div>

	<div class="tab-content">
		<div class="tab-pane tabs-animation fade show active" id="tab-content-0" role="tabpanel">
			<div class="row">
				<div class="col-md-8 m-auto">
					<div class="main-card mb-3 card">
						<div class="card-body">
							<h5 class="card-title">Add Post</h5>
							<form class="" action="{{ route('admin.post.store') }}" method="POST" enctype="multipart/form-data">
								@csrf
								<div class="position-relative form-group">
									<label for="exampleEmail" class="">Post Title</label>
									<input name="title" id="exampleEmail" placeholder="Enter post name" type="text" class="form-control" required>
								</div>
								<div class="position-relative form-group">
									<label for="exampleSelect" class="">Select Category</label>
									@if(count($post_category) > 0)
										<select class="form-control multiCatSelect" name="post_category[]" id="exampleSelect" multiple required>
											@foreach($post_category as $pCategory)
											<option value="{{$pCategory->id}}">{{ $pCategory->category_title }}</option>
											@endforeach
										</select>
									@endif
								</div>
                                <div class="position-relative form-group">
									<label for="exampleEmail" class="">Description</label>
                                    <textarea id="HCKEditor" name="description" id="exampleText" class="form-control" cols="30" rows="5" required></textarea>
								</div>

								<div class="position-relative form-group">
									<label for="exampleSelect" class="">Status</label>
									<select name="status" id="exampleSelect" class="form-control" required>
										<option value="0">Unpublish</option>
										<option selected value="1">Publish</option>
									</select>
								</div>

								<div class="position-relative form-group border-0">
					                <label class="mb-0">Cover Photo</label>
					                <div class="avatar-upload top-photo">
					                    <small class="form-text text-muted mt-0">File Size Must Be Less Than 1 MB</small>
					                    <div class="product-cover-photo-items-wrapper">
					                        <div class="product-cover-photo">
					                            <input class="p-cover-photo" type="file" accept="image/*" name="post_image[]" required>
					                            <div class="check-wrapper">
					                                <input class="product-cover-default-checkbox" type="checkbox" name="default_image[]" checked>
					                                <div class="box"></div>
					                                <label>Make As Default</label>
					                            </div>
					                            <div class="p-image-new-item-add-btn"><i class="fas fa-plus"></i>Add More Photo</div>
					                        </div>
					                    </div>
					                </div>
					            </div>
								<button class="mt-1 btn btn-primary p-store-btn">Submit</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@stop

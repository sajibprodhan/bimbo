@extends('layouts.admin')
@section('content')
@section('styles')
<style>
    .demo-image {
        display: flex;
        align-items: center;
        justify-content: center;
        margin-bottom: 33px;
    }
    .demo-image img {
        width: 43%;
    }
</style>
@stop
<div class="app-main__inner">
	@include('admin._partials.page-title')
    <div class="demo-image">
	    <img src="{{ asset("/") }}assets/admin/images/dashboard-image.png">
    </div>

</div>
@stop

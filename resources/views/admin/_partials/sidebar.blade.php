<div class="app-sidebar sidebar-shadow">
    <div class="app-header__logo">
        <div class="logo-src"></div>
        <div class="header__pane ml-auto">
            <div>
                <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar"> <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                </span>
                </button>
            </div>
        </div>
    </div>
    <div class="app-header__mobile-menu">
        <div>
            <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                </span>
            </button>
        </div>
    </div>
    <div class="app-header__menu">
        <span>
            <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                <span class="btn-icon-wrapper">
                    <i class="fa fa-ellipsis-v fa-w-6"></i>
                </span>
            </button>
        </span>
    </div>
    <div class="scrollbar-sidebar">
        <div class="app-sidebar__inner">
            <ul class="vertical-nav-menu">

                @php

                    // $heroSliderEdit  = strpos($_SERVER['REQUEST_URI'], '/admin/hero-slider/');
                @endphp

                <li>
                    <a href="{{route('admin.dashboard')}}" class="{{ request()->is('admin') ? 'mm-active' : '' }}">
                        <i class="metismenu-icon fas fa-tachometer-alt"></i>
                        Dashboard
                    </a>
                </li>

                <li class="sm-link">
                    <a href="#" class="">

                        <i class="metismenu-icon fas fa-clipboard-list"></i>
                        Categories
                        <i class="metismenu-state-icon fas fa-chevron-down"></i>
                    </a>
                    <ul>
                        <li>
                            <a href="{{ route('admin.category.create') }}" class="{{ request()->is('admin/category/create') ? 'mm-active' : '' }}">
                                <i class="metismenu-icon"></i>
                                Add Category
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('admin.category.index') }}" class="{{ request()->is('admin/category') ? 'mm-active' : '' }}">
                                <i class="metismenu-icon fas fa-clipboard-list"></i>
                                All Category
                            </a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="{{ route('admin.category.index') }}" class="{{ request()->is('admin/category') ? 'mm-active' : '' }}">
                        <i class="metismenu-icon fas fa-clipboard-list"></i>
                        All Category
                    </a>
                </li>
                    
            
                <li class="sm-link">
                    <a href="#" class="">

                        <i class="metismenu-icon fas fa-clipboard-list"></i>
                        Posts
                        <i class="metismenu-state-icon fas fa-chevron-down"></i>
                    </a>
                    <ul>
                        <li>
                            <a href="" class="{{ request()->is('admin/posts') ? 'mm-active' : '' }}">
                                <i class="metismenu-icon"></i>
                                All Posts
                            </a>
                        </li>
                        <li>
                            <a href=""  class="{{ request()->is('admin/post/create') ? 'mm-active' : '' }}">
                                <i class="metismenu-icon pe-7s-mouse"></i>
                            Add New Post</a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="{{ route('admin.all-images') }}" class="{{ request()->is('admin/all-images') ? 'mm-active' : '' }}">
                        <i class="metismenu-icon fas fa-clipboard-list"></i>
                        All Image List
                    </a>
                </li>
                

                <li class="sm-link">
                    <a href="#">
                        <i class="metismenu-icon fas fa-clipboard-list"></i>
                        Photo & Video Gallery
                        <i class="metismenu-state-icon fas fa-chevron-down"></i>
                    </a>
                    <ul>
                        <li>
                            <a href="{{route('admin.photo-gallery')}}" class="{{ request()->is('admin/photo-gallery') ? 'mm-active' : '' }}">
                                <i class="metismenu-icon"></i>
                                Photo Gallery
                            </a>
                        </li>
                        <li>
                            <a href="{{route('admin.video-gallery')}}" class="{{ request()->is('admin/video-gallery') ? 'mm-active' : '' }}">
                                <i class="metismenu-icon"></i>
                                Video Gallery
                            </a>
                        </li>
                    </ul>
                </li>
   
                <li>
                    <a href="" class="{{ request()->is('admin/home-intro') ? 'mm-active' : '' }}">
                         <i class="metismenu-icon fas fa-clipboard-list"></i>
                        Settings
                    </a>
                </li>                    
            
            </ul>
        </div>
    </div>
</div>

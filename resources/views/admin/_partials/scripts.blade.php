{{-- <script src="http://maps.google.com/maps/api/js?sensor=true"></script> --}}

<script type="text/javascript" src="{{ asset('assets/admin/scripts/main.js/') }}"></script>


<script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js "></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="https://cdn.ckeditor.com/4.15.1/standard/ckeditor.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{ asset('assets/toastr/js/toastr.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/admin/scripts/custom.js/') }}"></script>
<script type="text/javascript">
    // toastr script
    @if (Session::has('message'))
        toastr.success('{{Session::get('message')}}');
    @elseif(Session::has('error'))
        toastr.error('{{Session::get('error')}}');

    @elseif($errors)
        @foreach ($errors->all() as $error)
            toastr.error('{{$error}}');
        @endforeach
    @endif
</script>


<script>

    // $(document).ready(function () {
    //     $('table tbody').sortable({
    //         update: function (event, ui) {
    //             $(this).find('tr').each(function (index) {
    //                 if ($(this).attr('data-position') != (index+1)) {
    //                     $(this).attr('data-position', (index+1)).addClass('updated');
    //                 }
    //             });
    //             saveNewPositions();
    //         }
    //     });
    // });

    // function saveNewPositions() {
    //     var positions = [];
    //     $('.updated').each(function () {
    //         positions.push([$(this).attr('data-index'), $(this).attr('data-position')]);
    //         $(this).removeClass('updated');
    //     });

    //     $.ajax({
    //         url: '',
    //         method: 'GET',
    //         dataType: 'text',
    //         data: {
    //             update: 1,
    //             positions: positions
    //         }, 
    //         success: function (response) {
    //             console.log(response);
    //         }
    //     });
    // }

</script>

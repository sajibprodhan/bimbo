@extends('layouts.admin')
@section('title','Add Category')
@section('content')
	<div class="app-main__inner">
		<div class="app-page-title">
			<div class="page-title-wrapper">
				<div class="page-title-heading">
					<div class="page-title-icon"> <i class="pe-7s-display1 icon-gradient bg-premium-dark"></i>
					</div>
					<div>Add Category
					</div>
				</div>
			</div>
		</div>

		<div class="tab-content">
			<div class="tab-pane tabs-animation fade show active" id="tab-content-0" role="tabpanel">
				<div class="row">
					<div class="col-md-8 m-auto">
						<div class="main-card mb-3 card">
							<div class="card-body">
								<h5 class="card-title">Add Category</h5>
								<form class="" action="{{ route('admin.category.store') }}" method="POST" enctype="multipart/form-data">
									@csrf

									<div class="position-relative form-group">
										<label for="exampleFile" class="">Cover Image</label>
										<input name="cover" id="exampleFile" type="file" class="form-control-file" required>
									</div>

									<div class="position-relative form-group">
										<label for="exampleEmail" class="">Category Title</label>
										<input name="title" id="exampleEmail" placeholder="Enter category title" type="text" class="form-control">
									</div>

									<div class="position-relative form-group">
										<label for="exampleEmail" class="">Trailer</label>
										<input name="trailer" id="exampleEmail" placeholder="Enter triler" type="url" class="form-control">
									</div>

									<div class="position-relative form-group">
										<label for="HCKEditor" class="">Description</label>
										<textarea name="description" id="HCKEditor" class="form-control" cols="30" rows="5"></textarea>
									</div>

									<div class="position-relative form-group">
										<label for="exampleSelect" class="">Status</label>
										<select name="status" id="exampleSelect" class="form-control">
											<option value="0">Unpublish</option>
											<option selected value="1">Publish</option>
										</select>
									</div>

									<div class="position-relative form-group">
										<label for="view_priority" class="">View Priority</label>
										<input name="view_priority" id="view_priority" placeholder="Enter priority" type="text" class="form-control">
									</div>

									<button class="mt-1 btn btn-primary">Submit</button>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop

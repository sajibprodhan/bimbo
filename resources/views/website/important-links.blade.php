@extends('layouts.website')
@section('content')

    <!-- Important Link Section Start-->
    <section class="all-important-link-section p-0">
        <div class="container">
            <div class="row pb-4">
                <div class="col-md-4 all-important-link-outer">
                    <div class="all-important-link-wrapper organization">
                        <div class="heading">
                            <span class="icon"><i class="fas fa-users"></i></span>
                            <h2 class="title">Other Foreign Association's</h2>
                        </div>
                        <ul class="all-important-link-item-wrapper organization">
                            <li class="all-important-link-item">
                                <a class="link" target="_blank" href="https://fpc.state.gov/">Foreign Press Center</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank" href="http://www.un.org/en/media/accreditation/">UN Media Accreditation</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank" href="https://cpj.org/">CPJ</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank" href="https://cpj.org/asia/bangladesh/">CPJ Bangladesh</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank" href="http://www.un.org/news/">UN News and Media</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank" href="https://www.nypressclub.org/">New York Press Club</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank" href="http://www.jpcbd.org/">National Press Club, Dhaka</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank" href="http://unca.com/">UN Correspondents Association</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank" href="http://www.presscouncil.gov.bd/">Bangladesh Press Council</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="col-md-4 all-important-link-outer">
                    <div class="all-important-link-wrapper sites">
                        <div class="heading">
                            <span class="icon"><i class="fas fa-link"></i></span>
                            <h2 class="title">Important Website's</h2>
                        </div>
                        <ul class="all-important-link-item-wrapper">
                            <li class="all-important-link-item">
                                <a class="link" target="_blank" href="http://www.bangladesh.gov.bd/">বাংলাদেশ সরকার</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank" href="http://www.bdembassyusa.org/">দূতাবাস DC</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank" href="http://www.pmo.gov.bd/">প্রধানমন্ত্রী</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank" href="http://bdcgny.org/NY">নিউইয়র্ক কন্স্যুলেট</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank" href="http://www.mofa.gov.bd/">পররাষ্ট্র মন্ত্রণালয়</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank" href="http://www.bangladeshconsulatela.com/">লস এঞ্জেলেস কন্স্যুলেট</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank" href="http://www.probashi.gov.bd/">প্রবাসীকল্যাণ মন্ত্রণালয়</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank" href="http://dhaka.usembassy.gov/">ইউএস এম্বেসি ঢাকা</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank" href="http://www.forms.gov.bd/">ই-সিটিজেন, বাংলাদেশ</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank" href="http://www.uscis.gov/">ইউএস ইমিগ্রেশন</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank" href="http://www.bangladesh.gov.bd/">জাতীয় তথ্য বাতায়ন</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank" href="https://www.irs.gov/">আইআরএস</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank" href="http://www.bangladeshconsulatela.com/">বাংলাদেশ বিমান</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank" href="https://nybdpressclub.org/weather.com/?cm_ven=PS_GGL_Weather_9102015_1">আবহাওয়া</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank" href="https://nybdpressclub.org/www.dsebd.org?cm_ven=PS_GGL_Weather_9102015_1">শেয়ার মার্কেট DSE</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4 all-important-link-outer">
                    <div class="all-important-link-wrapper newspaper">
                        <div class="heading">
                            <span class="icon"><i class="far fa-newspaper"></i></span>
                            <h2 class="title">Print & Online Media(USA)</h2>
                        </div>
                        <ul class="all-important-link-item-wrapper">
                            <li class="all-important-link-item">
                                <a class="link" target="_blank"href="http://www.banglapatrikausa.com/">সাপ্তাহিক বাংলা পত্রিকা</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank"href="http://www.banglatimes.com/">সাপ্তাহিক দেশবাংলা</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank"href="https://www.prothomalo.com/northamerica">সাপ্তাহিক প্রথম আলো-আমেরিকা</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank"href="http://www.banglatimes.com/">সাপ্তাহিক বাংলা টাইমস</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank"href="http://bangla.bornomalanews.com/">সাপ্তাহিক বর্ণমালা</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank"href="http://timetvusa.com/">টাইম টিভি</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank"href="http://hakkatha.com/">হক কথা</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank"href="http://ajkalnewyork.com/">সাপ্তাহিক আজকাল</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank"href="http://www.thikana.us/">সাপ্তাহিক ঠিকানা</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank"href="http://weeklybangalee.com/">সাপ্তাহিক বাঙালী</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank"href="https://www.facebook.com/Weekly-Parichoy-310125195776956/">সাপ্তাহিক পরিচয়</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank"href="http://www.newsprobash.com/">সাপ্তাহিক প্রবাস</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank"href="banews24.com">বিএনিউজ২৪.কম </a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank"href="http://www.banglapress.com/">বাংলা প্রেস</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank"href="http://probashnews.com/">প্রবাস নিউজ</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank"href="http://khabor.com/">খবর</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Important Link Section End-->

@endsection

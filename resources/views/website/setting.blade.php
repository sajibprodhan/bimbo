@extends('layouts.admin')
@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon"> <i class="pe-7s-display1 icon-gradient bg-premium-dark"></i>
                </div>
                <div>
                    Edit Setting
                </div>
            </div>
        </div>
    </div>
    <div class="tab-content">
        <div class="tab-pane tabs-animation fade show active" id="tab-content-0" role="tabpanel">
            <div class="row">
                <div class="col-md-8 m-auto">
                    <div class="main-card mb-3 card">
                        <div class="card-body">
                            <h5 class="card-title">Edit Settings</h5>
                            <form class="" action="" method="POST" enctype="multipart/form-data">
                                @csrf

                                <div class="position-relative form-group">
                                    <label class="">Favicon Icon</label>
                                    <input class="form-control" type="file" name="favicon_icon">
                                    {{-- @if($home_intro_data->image)
                                        <img class="mt-2" style="max-width: 100px;" src="{{asset($home_intro_data->image)}}">
                                    @endif --}}
                                </div>

                                <div class="position-relative form-group">
                                    <label class="">Header Logo</label>
                                    <input class="form-control" type="file" name="header_logo">
                                    {{-- @if($home_intro_data->image)
                                        <img class="mt-2" style="max-width: 100px;" src="{{asset($home_intro_data->image)}}">
                                    @endif --}}
                                </div>

                                <div class="position-relative form-group">
                                    <label class="">Admin Header Logo</label>
                                    <input class="form-control" type="file" name="admin_header_logo">
                                    {{-- @if($home_intro_data->image)
                                        <img class="mt-2" style="max-width: 100px;" src="{{asset($home_intro_data->image)}}">
                                    @endif --}}
                                </div>

                                <div class="position-relative form-group">
                                    <label class="">Home Banner Image</label>
                                    <input class="form-control" type="file" name="home_banner">
                                    {{-- @if($home_intro_data->image)
                                        <img class="mt-2" style="max-width: 100px;" src="{{asset($home_intro_data->image)}}">
                                    @endif --}}
                                </div>

                                <div class="position-relative form-group">
                                    <label class="">Footer Logo</label>
                                    <input class="form-control" type="file" name="footer_logo">
                                    {{-- @if($home_intro_data->image)
                                        <img class="mt-2" style="max-width: 100px;" src="{{asset($home_intro_data->image)}}">
                                    @endif --}}
                                </div>

                                <div class="position-relative form-group">
                                    <label for="exampleEmail" class="">Address</label>
                                    <textarea id="HCKEditor" name="address" id="exampleText" class="form-control" cols="30" rows="5" required></textarea>
                                </div>

                                <div class="position-relative form-group">
                                    <label class="">Phone-1</label>
                                    <input class="form-control" type="text" name="phone_1" value="">
                                </div>

                                <div class="position-relative form-group">
                                    <label class="">Phone-2</label>
                                    <input class="form-control" type="text" name="phone_2" value="">
                                </div>

                                <div class="position-relative form-group">
                                    <label class="">Email-1</label>
                                    <input class="form-control" type="text" name="email_1" value="">
                                </div>

                                <div class="position-relative form-group">
                                    <label class="">Email-2</label>
                                    <input class="form-control" type="text" name="email_2" value="">
                                </div>

                                <div class="position-relative form-group">
                                    <label class="">Facebook Url</label>
                                    <input class="form-control" type="url" name="facebook_url" value="">
                                </div>

                                <div class="position-relative form-group">
                                    <label class="">Instagram Url</label>
                                    <input class="form-control" type="url" name="instagram_url" value="">
                                </div>

                                <div class="position-relative form-group">
                                    <label class="">Youtube Url</label>
                                    <input class="form-control" type="url" name="youtube_url" value="">
                                </div>

                                <div class="position-relative form-group">
                                    <label class="">Whatsapp_url Url</label>
                                    <input class="form-control" type="url" name="whatsapp_url" value="">
                                </div>
                           
                                <button class="mt-1 btn btn-primary">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

</script>
@stop

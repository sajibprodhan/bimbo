<!-- Bootstrap Popper JS -->
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<!-- Slick JS -->
<script src="{{asset('assets/website/slick-slider/slick.min.js')}}"></script>
<!-- Owl Carousel JS -->
<script src="{{asset('assets/website/owl-carousel/owl.carousel.min.js')}}"></script>
<!-- Marquee CDN -->
<script src="https://www.jqueryscript.net/demo/Text-Scrolling-Plugin-for-jQuery-Marquee/jquery.marquee.js?v=3" type="text/javascript"></script>
<!-- Grid Gallery Js -->
<script src="{{asset('assets/website/grid-gallery/GridHorizontal.js')}}"></script>
<script src="{{asset('assets/website/grid-gallery/imagesloaded.pkgd.min.js')}}"></script>
<!-- Progressbar js -->
<script src="{{asset('assets/website/js/progressbar.js')}}"></script>
<!-- Lightbox Js -->
<script src="{{asset('assets/website/grid-gallery/lightbox.js')}}"></script>
<script src="{{ asset('assets/toastr/js/toastr.min.js') }}"></script>
<!-- Main JS -->
<script type="text/javascript" src="{{asset('assets/website/js/main.js')}}"></script>
<script type="text/javascript">
    // toastr script
    @if (Session::has('message'))
        toastr.success('{{Session::get('message')}}');
    @elseif(Session::has('success'))
        toastr.error('{{Session::get('success')}}');
    @elseif(Session::has('error'))
        toastr.error('{{Session::get('error')}}');
    @elseif($errors)
        @foreach ($errors->all() as $error)
        toastr.error('{{$error}}');
        @endforeach
    @endif



    // Checkout Page Terms Condition Checkbox
    if($('#termsConditionAgreement').length > 0){
        $('button').click(function(e){
            if($('#termsConditionAgreement').is(':checked')){
                $('.agreement-error-msg').remove();
            }else{
                e.preventDefault();
                $('.agreement-error-msg').remove();
                $('#termsConditionAgreement').closest('div').append('<p class="agreement-error-msg">You Have To Agree With Terms & Conditions</p>');
            }
        });
        $('#termsConditionAgreement').click(function(){
            if($(this).is(':checked')){
                $('.agreement-error-msg').remove();
            }else{
                $('.agreement-error-msg').remove();
                $(this).closest('div').append('<p class="agreement-error-msg">You Have To Agree With Terms & Conditions</p>');
            }
        });
    }
</script>


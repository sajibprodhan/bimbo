<footer class="footer-main">
	<div class="container">
		<div class="footer-top-box">
			<div class="row">
				<div class="col-md-3">
					<div class="footer-nav-item-wrapper">
						<div class="footer-logo-box">
							<div class="link">
								<img src="{{asset('assets/logo/white-logo-1.png')}}" style="width: 95px">
							</div>
							<div class="short-descriptiopn footer_short_desc">
								{!! Str::limit($g_about_us->profile_description, 200).'...' !!}
							</div>
							<a class="article-read-more" href="{{ route('about-us-content') }}">Read More</a>

                            {{-- <a class="article-read-more" href="{{ route('single-footer-detais', $g_about_us->id) }}">Read More</a> --}}
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="footer-nav-item-wrapper">
						<div class="main-heading">
							<h2 class="main-title">Contact Us</h2>
						</div>
						<!-- Phone Number Start -->
						<ul class="footer-nav-item-inner">


							<li class="footer-nav-item">
                                <div>
                                    <span class="icon"><i class="fas fa-phone"></i></span>
                                    <a class="link" href="tel:{{$g_about_us->phone}}">{{$g_about_us->phone}}</a>
                                </div>

                                <div>
                                    <span class="icon"><i class="fas fa-phone"></i></span>
                                    <a class="link" href="tel:{{ $g_about_us->phone2 }}2">{{ $g_about_us->phone2 }}</a>
                                </div>

                                <div>
                                    <span class="icon"><i class="fas fa-phone"></i></span>
                                    <a class="link" href="tel:{{ $g_about_us->phone3 }}">{{ $g_about_us->phone3 }}</a>
                                </div>
                            </li>

						</ul>
						<!-- Phone Number End -->
						<!-- Email Address Start-->
						<ul class="footer-nav-item-inner email">
							<li class="footer-nav-item">
								<span class="icon"><i class="far fa-envelope"></i></span>
								<a class="link" href="mailto:{{$g_about_us->email}}">{{$g_about_us->email}}</a>
                            </li>


						</ul>
						<!-- Email Address End -->
						<!-- Location Start-->
						<ul class="footer-nav-item-inner email">
							<li class="footer-nav-item address">
								<span class="icon"><i class="fas fa-map-marker-alt"></i></span>
								<div class="link address-location">
									{!! $g_about_us->address !!}
								</div>
							</li>
						</ul>
						<!-- Location End -->
						<!-- Website Start-->
						<!-- <div class="heading">
							<h2 class="title">Website</h2>
						</div>
						<ul class="footer-nav-item-inner website">
							<li class="footer-nav-item">
								<span class="icon"><i class="fas fa-globe"></i></span>
								<a class="link" href="https://nybdpressclub.org">www.yourwebsite.com/</a>
							</li>
						</ul> -->
						<!-- Website End -->
					</div>
				</div>
				<!-- Address Start -->
				<div class="col-md-3">
					<div class="footer-nav-item-wrapper">
						<!-- Quick Links Start -->
						<div class="main-heading">
							<h2 class="main-title">Quick Links</h2>
						</div>
						<ul class="footer-nav-item-inner quick-links border-0">

							<li class="footer-nav-item">
								<a class="link" href="{{ route('applicationfrom') }}">Application Form</a>
                            </li>

                            <li class="footer-nav-item">
                                <a class="link" href="{{ route('media-link') }}">Media Link</a>
                            </li>

							<li class="footer-nav-item">
                                <a class="link" href="{{ route('important-links') }}">Important Link</a>
                            </li>

                            <li class="footer-nav-item social-follow">
								<a class="follow-link donate-btn" href="{{ route('donate') }}">
									<img src="{{asset('assets/website/image/donate.png')}}">
								</a>
							</li>

						</ul>
						<!-- Quick Links End -->
					</div>
				</div>
				<!-- Address End -->
				<div class="col-md-3">
					<div class="footer-nav-item-wrapper">
						<!-- Quick Links Start -->
						<div class="main-heading">
							<h2 class="main-title">Quick Links</h2>
						</div>
						<ul class="footer-nav-item-inner quick-links border-0">

							<li class="footer-nav-item">
								<a class="link" href="{{ route('privacy-policy') }}">Privacy Policy</a>
                            </li>

                            <li class="footer-nav-item">
                                <a class="link" href="{{ route('term-of-service') }}">Terms of Services</a>
                            </li>

                            <li class="footer-nav-item">
                                <a class="link" href="{{ route('category-news', 9) }}">আলোচিত সংবাদ</a>
                            </li>


							<li class="footer-nav-item social-follow">
								<a class="follow-link" target="_blank" href="https://www.facebook.com/">
									<img src="{{asset('assets/website/image/fb.png')}}">
								</a>
							</li>
						</ul>

						<!-- Quick Links End -->
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="copyright">
		<div class="container">
			<div class="row">
				<div class="col-md-12 copyright-left-box">
					Copyright © <a class="link" href="/">{{ str_replace('_', ' ', config('app.name')) }}</a> {{date("Y")}}. Website Developed & Maintenance by <a class="link company-logo" href="https://bijoytech.com/"><img src="{{asset('assets/logo/bt-logo-01.png')}}"></a>
				</div>
			</div>
		</div>
	</div>
</footer>

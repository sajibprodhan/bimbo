<!-- Header Start-->

<header class="header-main {{ request()->is('/') ? 'home' : '' }}">
	<div class="header-inner-wrapper">
		<!-- Navbar Main Section -->
		<nav class="navbar-main">
			<div class="top-header">
				<div class="logo-section">
					<a class="brand-logo" href="{{ url('/') }}">
						<img class="laptop" src="{{ asset('/assets/') }}/logo/logo-01.png">
						<img class="mobile-tab" src="{{ asset('/assets/') }}/logo/logo-01.png">
					</a>

					<button class="navbar-menu-toggle-btn">
						<span class="toggler-icon-bar"></span>
					</button>
				</div>
			</div>

			<div class="nav-items-wrapper">
				<ul class="nav-item-inner-wrapper">
                    <li class="nav-item-main">
                        <a class="nav-item-link" href="{{ route('home') }}">Home</a>
                    </li>

                    <li class="nav-item-main item-has-submenu">
                        <a class="nav-item-link" href="{{ route('about-us-content') }}">About Us</a>
                        <ul class="nav-item-submenu">

                            <li class="nav-item-submenu-item">
                                <a class="nav-item-submenu-item-link" href="{{ route('our-missin') }}">Our Mission</a>
                            </li>

                            <li class="nav-item-submenu-item">
                                <a class="nav-item-submenu-item-link" href="{{ route('our-vission') }}">Our Vision</a>
                            </li>

                            <li class="nav-item-submenu-item">
                                <a class="nav-item-submenu-item-link" href="{{ route('constitutions') }}">Constitution</a>
                            </li>

                            <li class="nav-item-submenu-item">
                                <a class="nav-item-submenu-item-link" href="{{ route('member-pres-message') }}">Message From President</a>
                            </li>

                            <li class="nav-item-submenu-item">
                                <a class="nav-item-submenu-item-link" href="{{ route('member-sec-message') }}">Message From Secretary</a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-item-main item-has-submenu">
						<a class="nav-item-link" href="">Committee</a>
						<ul class="nav-item-submenu">
                            <li class="nav-item-submenu-item">
								<a class="nav-item-submenu-item-link" href="{{ route('trustee-board') }}">Board of Trustee</a>
							</li>

							<li class="nav-item-submenu-item">
								<a class="nav-item-submenu-item-link" href="{{ route('executive-committee') }}">Executive Committee</a>
							</li>

							<li class="nav-item-submenu-item">
								<a class="nav-item-submenu-item-link" href="{{ route('former-committee') }}">Former Committee</a>
							</li>
						</ul>
					</li>

                    <li class="nav-item-main item-has-submenu">
						<a class="nav-item-link" href="">Members</a>
						<ul class="nav-item-submenu">
							<li class="nav-item-submenu-item">
								<a class="nav-item-submenu-item-link" href="{{ route('general-member') }}">General Member</a>
							</li>

							<li class="nav-item-submenu-item">
								<a class="nav-item-submenu-item-link" href="{{ route('lifemembr') }}">Life Member</a>
							</li>
						</ul>
                    </li>


                    <li class="nav-item-main item-has-submenu">
                        <a class="nav-item-link" href="">Events/News</a>
                        <ul class="nav-item-submenu">
                            <li class="nav-item-submenu-item">
                                <a class="nav-item-submenu-item-link" href="{{ route('category-news',8) }}">Events/News</a>
                            </li>

                            <li class="nav-item-submenu-item">
                                <a class="nav-item-submenu-item-link" href="{{ route('category-news',10) }}">Obituary News</a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-item-main">
                        <a class="nav-item-link" href="{{ route('photo-gallery') }}">Photo Gallery</a>
                    </li>
                    <li class="nav-item-main">
                        <a class="nav-item-link" href="{{ route('video-gallery') }}">Video Gallery</a>
                    </li>

                    <li class="nav-item-main item-has-submenu right-view">
						<a class="nav-item-link" href="">Membership</a>
						<ul class="nav-item-submenu">

							<li class="nav-item-submenu-item">
								<a class="nav-item-submenu-item-link" href="{{ route('membership-overview') }}">Membership Overview</a>
                            </li>

							<li class="nav-item-submenu-item">
								<a class="nav-item-submenu-item-link" href="{{ route('applicationfrom') }}">Membership Application </a>
                            </li>

                            <li class="nav-item-submenu-item">
								<a class="nav-item-submenu-item-link" href="{{ route('membershiprenew') }}">Renew Membership Dues</a>
                            </li>

                            <li class="nav-item-submenu-item">
								<a class="nav-item-submenu-item-link" href="{{ route('register-plan') }}">Membership Packages</a>
                            </li>

							<li class="nav-item-submenu-item">
								<a class="nav-item-submenu-item-link" href="{{ route('updateinfo') }}">Update Your Info</a>
							</li>

                            @if ( ! Auth::guard('subscriber')->check() )
                                <li class="nav-item-submenu-item">
                                    <a class="nav-item-submenu-item-link" href="{{ route('membership.login') }}">Membership Login</a>
                                </li>
                            @else
                                 <li class="nav-item-submenu-item">
                                    <a class="nav-item-submenu-item-link" href="{{ route('subscribe-user-profile.index') }}">My Profile</a>
                                </li>
                            @endif

						</ul>
                    </li>

                    <li class="nav-item-main">
                        <a class="nav-item-link" href="{{ route('contact-us') }}">Contact</a>
                    </li>

                    <li class="nav-item-main">
                        <a class="nav-item-link" href="{{ route('spelling-bee') }}">Spelling Bee</a>
                    </li>

                    <li class="nav-item-main">
                        <a class="nav-item-link" href="{{ route('reading-competition') }}">Reading Competition</a>
                    </li>
				</ul>
			</div>
		</nav>
	</div>
</header>
<!-- Header End-->


@extends('layouts.website')
@section('content')

		<!-- About US Section Start -->
	<section class="about-us-main-info-section">
		<div class="container">
			<div class="main-heading mb-3">
				<h2 class="main-title">Juktarastra Habibonj Zila Samiti Inc.</h2>
			</div>

			<div class="membership-renew-info-wrapper">
				<div class="col-md-7 m-auto">
					<div class="details">
                       {!! $footerTextDetails->profile_description !!}
					</div>
				</div>
			</div>

		</div>
	</section>
	<!-- //About Us Section End -->

@endsection

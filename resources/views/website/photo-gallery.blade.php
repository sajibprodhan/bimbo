@extends('layouts.website')
@section('content')
	<!-- Image Grid Start -->
	<section class="image-gallery-grid-section">
		<div class="container">
			<div class="main-heading">
				<h2 class="main-title">Photo Gallery</h2>
				<!-- Preloader -->
				<div id="preloader">
				  <div id="status">&nbsp;</div>
				</div>
			</div>
            @if(count($g_photo_gallery_data) > 0)
			    <div class="image-grid-inner gallery" id="imageGridGallery">
					@foreach($g_photo_gallery_data as $photo_data)
						<div class="item">
						  <a href="{{asset($photo_data->image_url)}}">
						    <img src="{{asset($photo_data->image_url)}}" />
						  </a>
						  <h2 class="title">{{ $photo_data->title }}</h2>
						</div>
					@endforeach
			    </div>
                <div class="pagination-section">
                    {!! $g_photo_gallery_data->links() !!}
                </div>
            @endif

		</div>
	</section>
	<!-- Image Grid End -->
@endsection

@extends('layouts.website')
@section('content')
<!-- Main Intro Start-->


<!-- Main Slider Start-->
<section class="home-slider-section">
	<div class="home-main-slider-inner owl-carousel">
        @if ($sliders->isNotEmpty())
            @foreach ($sliders as $slider)
                <div class="item">
                    <img src="{{ asset($slider->image) }}" alt="images not found">
                     <div class="header-content">
                        <h2 class="title">{{ $slider->title }}</h2>
                        <h2 class="sliderSubtitle">To</h2>
                        <h2>{{ $slider->subtitle }}</h2>
                    </div>
                </div>
            @endforeach
        @endif
	</div>
</section>
<!-- Main Slider End-->

<!-- Headline scrolling Section start -->
<section class="main-headline-section">
    <div class="container marquees-outer">
        <h3 class="main-title">শিরোনাম</h3>
        <div class="marquees">
            @if(count($scrollablePosts) > 0)
                @foreach($scrollablePosts->take(20) as $scrollablePost)
                    <h4 class="item">
                        <a href="{{route('single-news',$scrollablePost->slug)}}">{{ $scrollablePost->title }}</a>
                    </h4>
                @endforeach
            @endif

        </div>
    </div>
</section>
<!-- Headline scrolling Section end -->

<!-- Home News section start -->
<section class="home-news-section">
    <div class="container">
        <div class="row all-box-wrapper">
            <div class="col-md-3 left-box">
                <div class="left-box-news-inner">
                    <div class="heading">
                        <a class="title" href="{{ route('category-news', 8) }}">সর্বশেষ</a>
                    </div>
                    <div class="short-news-items-wrapper">

                        @if(count($scrollablePosts) > 0)
                            @foreach($scrollablePosts as $scrollablePost)
                                <a class="latest-update-news-item" href="{{route('single-news',$scrollablePost->slug)}}">
                                    @if ($scrollablePost->PostImage)
                                        <div class="image">
                                        <img src="{{ asset($scrollablePost->PostImage->post_image) }}">
                                    </div>
                                    @endif

                                    <div class="content">
                                        <h2 class="title">{{ $scrollablePost->title }}</h2>
                                    </div>
                                </a>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-6 middle-box">
                <div class="home-main-news-slider-inner owl-carousel">
                    @if(count($scrollablePosts) > 0)
                        @foreach($scrollablePosts as $scrollablePost)
                            <a href="{{route('single-news',$scrollablePost->slug)}}" class="slider-item">
                                @if ($scrollablePost->PostImage)
                                    <div class="image">
                                        <img src="{{ asset($scrollablePost->PostImage->post_image) }}">
                                    </div>
                                @endif
                                <div class="content">
                                    <h2 class="title">{{ $scrollablePost->title }}</h2>
                                </div>
                            </a>
                        @endforeach
                    @endif
                </div>
            </div>
            <div class="col-md-3 left-box">
                <div class="left-box-news-inner">

                    <div class="short-news-items-wrapper">
                        <div class="addImage">
                            <a href="{{ route('apply-scholarship') }}"><img src="{{ asset('/') }}assets/website/image/baanner-scholar.jpg"></a>
                        </div>

                        <div class="progress-items">
                            <h4 class="main-title">Upcomming Events</h4>
                            <ul class='skills'>
                                <li class='progressbar'  data-width='80' data-target='80'>Program One</li>
                                <li class='progressbar'  data-width='90' data-target='90'>Key Tow</li>
                                <li class='progressbar'  data-width='70' data-target='70'>Let's Move On</li>
                                <!-- <li class='progressbar'  data-width='80' data-target='80'>Go Ahead</li> -->
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Home News section end -->



<!-- Donate Organization Section -->
<section class="donate-organization-setion">
	<div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="donate-organization-left-box">
                    <h2 class="subtitle">{{ $contribution->subtitle }}</h2>
                    <h2 class="title">{{ $contribution->title }}</h2>
                    <div class="details">
                        {!! $contribution->content !!}
                    </div>
                    <a class="donate-organization-button" href="{{ $contribution->url }}">Contribution</a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="donate-organization-middle-box">
                    <a href="{{ $contribution->url }}">
                        <img src="{{ asset($contribution->donate_one) }}">
                    </a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="donate-organization-right-box">
                    <a href="javascript:;">
                        <img src="{{ asset($contribution->donate_tow) }}">
                    </a>
                </div>
            </div>
        </div>
	</div>
</section>
<!-- Donate Organization Section -->




<!-- Events Start -->
<section class="latest-update-section">
	<div class="container">
		<div class="main-heading">
			<h2 class="main-title mb-4">News</h2>
		</div>
		<div class="latest-update-news-item-wrapper">
			<div class="row">
				@if(count($home_page_data['popular_news_posts']) > 0)
					@foreach($home_page_data['popular_news_posts'] as $popular_news_post)
					<div class="col-md-3 latest-update-news-item-outer">
						<a class="latest-update-news-item" href="{{route('single-news',$popular_news_post->slug)}}">
							@if($popular_news_post->PostImage)
                                <div class="image">
                                    <img src="{{asset($popular_news_post->PostImage->post_image)}}">
                                </div>
							@endif
							<div class="content">
								<h2 class="title">{{$popular_news_post->title}}</h2>
								<div class="meta">
									<h3 class="date">
										Published On </span>
										{{ date('M d, Y', strtotime($popular_news_post->created_at)) }}
									</h3>
								</div>
							</div>
						</a>
					</div>
					@endforeach
				@endif
			</div>
			<div class="see-more-btn-wrapper">
				<a class="see-more-btn" href="{{ route('category-news',8) }}">All Updates</a>
			</div>
		</div>
	</div>
</section>
<!-- Events End -->


<!-- Mission Membership Award Start-->
<section class="mission-membership-award-section">
    <div class="container">
        <div class="row">

            @forelse ($missions as $mission)
                <div class="col-md-4 our-mission-content-box-outer">
                    <div class="our-mission-content-box-wrapper">
                        <a class="our-mission-content-box-inner" href>
                            <div class="icon">
                                <span>{!! $mission->icon !!}</span>
                            </div>
                            <div class="main-content">
                                <h2 class="title">{{ ucfirst($mission->title) }}</h2>
                                {!! $mission->content !!}
                            </div>
                        </a>
                        <a class="read-more-btn" href="{{ $mission->url }}">Read More</a>
                    </div>
                </div>
            @empty
                <h4 class="text-center">Mission Post Not Found</h4>
            @endforelse
        </div>
    </div>
</section>
<!-- Mission Membership Award End-->





<!-- Important Link Section Start-->
<section class="all-important-link-section">
	<div class="container">
		<div class="row">
			<div class="col-md-4 all-important-link-outer">
				<div class="all-important-link-wrapper organization">
					<div class="heading">
						<span class="icon"><i class="fas fa-users"></i></span>
						<h2 class="title">Other Organization Association's</h2>
					</div>
					<ul class="all-important-link-item-wrapper organization">
                        @forelse ($organization_associations as $organization_association)
                            <li class="all-important-link-item">
                                <a class="link" target="_blank" href="{{ $organization_association->link }}">{{ $organization_association->link_heading }}</a>
                            </li>
                        @empty
                            <li class="all-important-link-item">
                                <a class="link" target="_blank" href="https://fpc.state.gov/">Foreign Press Center</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank" href="http://www.un.org/en/media/accreditation/">UN Media Accreditation</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank" href="https://cpj.org/">CPJ</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank" href="https://cpj.org/asia/bangladesh/">CPJ Bangladesh</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank" href="http://www.un.org/news/">UN News and Media</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank" href="https://www.nypressclub.org/">New York Press Club</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank" href="http://www.jpcbd.org/">National Press Club, Dhaka</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank" href="http://unca.com/">UN Correspondents Association</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank" href="http://www.presscouncil.gov.bd/">Bangladesh Press Council</a>
                            </li>
                        @endforelse
					</ul>
				</div>
            </div>

			<div class="col-md-4 all-important-link-outer">
				<div class="all-important-link-wrapper sites">
					<div class="heading">
						<span class="icon"><i class="fas fa-link"></i></span>
						<h2 class="title">Important Website's</h2>
					</div>
					<ul class="all-important-link-item-wrapper">

                        @forelse ($important_websites as $important_website)
                            <li class="all-important-link-item">
                                <a class="link" target="_blank" href="{{ $important_website->link }}">{{ $important_website->link_heading }}</a>
                            </li>
                        @empty
                            <li class="all-important-link-item">
                                <a class="link" target="_blank" href="http://www.bangladesh.gov.bd/">বাংলাদেশ সরকার</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank" href="http://www.bdembassyusa.org/">দূতাবাস DC</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank" href="http://www.pmo.gov.bd/">প্রধানমন্ত্রী</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank" href="http://bdcgny.org/NY">নিউইয়র্ক কন্স্যুলেট</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank" href="http://www.mofa.gov.bd/">পররাষ্ট্র মন্ত্রণালয়</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank" href="http://www.bangladeshconsulatela.com/">লস এঞ্জেলেস কন্স্যুলেট</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank" href="http://www.probashi.gov.bd/">প্রবাসীকল্যাণ মন্ত্রণালয়</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank" href="http://dhaka.usembassy.gov/">ইউএস এম্বেসি ঢাকা</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank" href="http://www.forms.gov.bd/">ই-সিটিজেন, বাংলাদেশ</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank" href="http://www.uscis.gov/">ইউএস ইমিগ্রেশন</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank" href="http://www.bangladesh.gov.bd/">জাতীয় তথ্য বাতায়ন</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank" href="https://www.irs.gov/">আইআরএস</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank" href="http://www.bangladeshconsulatela.com/">বাংলাদেশ বিমান</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank" href="https://nybdpressclub.org/weather.com/?cm_ven=PS_GGL_Weather_9102015_1">আবহাওয়া</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank" href="https://nybdpressclub.org/www.dsebd.org?cm_ven=PS_GGL_Weather_9102015_1">শেয়ার মার্কেট DSE</a>
                            </li>
                        @endforelse
					</ul>
				</div>
			</div>
			<div class="col-md-4 all-important-link-outer">
				<div class="all-important-link-wrapper newspaper">
					<div class="heading">
						<span class="icon"><i class="far fa-newspaper"></i></span>
						<h2 class="title">Print & Online Media (USA)</h2>
					</div>
					<ul class="all-important-link-item-wrapper">
                        @forelse ($print_medias as $print_media)
                            <li class="all-important-link-item">
                                <a class="link" target="_blank" href="{{ $print_media->link }}">{{ $print_media->link_heading }}</a>
                            </li>
                        @empty
                            <li class="all-important-link-item">
                                <a class="link" target="_blank"href="http://www.banglapatrikausa.com/">সাপ্তাহিক বাংলা পত্রিকা</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank"href="http://www.banglatimes.com/">সাপ্তাহিক দেশবাংলা</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank"href="https://www.prothomalo.com/northamerica">সাপ্তাহিক প্রথম আলো-আমেরিকা</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank"href="http://www.banglatimes.com/">সাপ্তাহিক বাংলা টাইমস</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank"href="http://bornomalanews.com/">সাপ্তাহিক বর্ণমালা</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank"href="http://timetvusa.com/">টাইম টিভি</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank"href="http://hakkatha.com/">হক কথা</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank"href="http://ajkalnewyork.com/">সাপ্তাহিক আজকাল</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank"href="http://www.thikana.us/">সাপ্তাহিক ঠিকানা</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank"href="http://weeklybangalee.com/">সাপ্তাহিক বাঙালী</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank"href="https://www.facebook.com/Weekly-Parichoy-310125195776956/">সাপ্তাহিক পরিচয়</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank"href="http://www.newsprobash.com/">সাপ্তাহিক প্রবাস</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank"href="https://bangla.bdnews24.com/">বিএনিউজ২৪.কম </a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank"href="http://www.banglapress.com/">বাংলা প্রেস</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank"href="http://probashnews.com/">প্রবাস নিউজ</a>
                            </li>
                            <li class="all-important-link-item">
                                <a class="link" target="_blank"href="http://khabor.com/">খবর</a>
                            </li>
                        @endforelse
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Important Link Section End-->



<!-- Popular Newspaper links section start -->
<section class="popular-newspaper-section">
	<div class="container">
		<div class="heading">
			<h2 class="title">
				<span class="icon"><i class="far fa-newspaper"></i></span>
				Print & Online Media (BD)
			</h2>
		</div>
		<div class="main-content">
			@if(count($g_print_and_news_all_data) > 0)
				@foreach($g_print_and_news_all_data as $print_and_news_item)
					<a target="_blank" href="{{$print_and_news_item->link}}" class="news-item">
						<img src="{{asset($print_and_news_item->image)}}">
					</a>
				@endforeach
			@endif
		</div>
	</div>
</section>
<!-- Popular Newspaper links section end -->

<!-- Home Photo and Video  Gallery Start -->
<section class="home-photo-and-video-gallery-section">
	<div class="container">
		<div class="row">
			<div class="col-md-6 photo-video-gallery-outer photo">
				<div class="heading">
					<span class="icon"><i class="fas fa-image"></i></span>
					<h2 class="title">Photo Gallery</h2>
				</div>
				<div class="photo-video-gallery-inner">
					<!-- Slider Start -->
				  	<div class="slider slider-for">
				  		@if(count($g_photo_gallery_data) > 0)
					  		@foreach($g_photo_gallery_data->take(8) as $photo_data)
						    <div>
						    	<img src="{{asset($photo_data->image_url)}}">
						    </div>
							@endforeach
						@endif
				  	</div>
				  	<div class="slider slider-nav">
					    @if(count($g_photo_gallery_data) > 0)
					  		@foreach($g_photo_gallery_data->take(8) as $photo_data)
						    <div>
						    	<img src="{{asset($photo_data->image_url)}}">
						    </div>
							@endforeach
						@endif
				  	</div>
					<!-- Slider End -->
					<div class="see-more-btn-wrapper">
						<a class="see-more-btn" href="{{ route('photo-gallery') }}">Show More</a>
					</div>
				</div>
			</div>
			<div class="col-md-6 photo-video-gallery-outer video">
				<div class="heading">
					<span class="icon"><i class="fas fa-video"></i></span>
					<h2 class="title">Video Gallery</h2>
				</div>
				<div class="photo-video-gallery-inner">
					<!-- Slider Start -->
				  	<div class="slider slider-for">
					    @if(count($g_video_gallery_data) > 0)
					  		@foreach($g_video_gallery_data->take(4) as $video_data)
						    <div>
						    	{!! $video_data->video_url !!}
						    </div>
							@endforeach
						@endif
				  	</div>
				  	<div class="slider slider-nav">
					    @if(count($g_video_gallery_data) > 0)
					  		@foreach($g_video_gallery_data->take(4) as $video_data)
						    <div>
						    	{!! $video_data->video_url !!}
						    </div>
							@endforeach
						@endif
				  	</div>
					<!-- Slider End -->
					<div class="see-more-btn-wrapper">
						<a class="see-more-btn" href="{{ route('video-gallery') }}">Show More</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Home Photo and Video  Gallery End -->




	<!-- A-banner Section Start -->
	<section class="a-banner-section">
		<div class="container">
			<div class="row">

                <div class="col-md-6 a-banner-slider-wrapper">
                    <div class="a-banner-slider-inner left owl-carousel owl-theme">
                        @if (count($firstBanners) > 0)
                            @foreach ($firstBanners->take(8) as $firstBanner)
                                <div class="a-banner-slider-item" data-dot="<button>{{ $loop->index+1 }}</button>">
                                    <img src="{{ asset($firstBanner->banner_img) }}">
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>

                <div class="col-md-6 a-banner-slider-wrapper">
                     <div class="a-banner-slider-inner left owl-carousel owl-theme">
                        @if (count($secondBanners) > 0)
                            @foreach ($secondBanners->take(8) as $secondBanner)
                                <div class="a-banner-slider-item" data-dot="<button>{{ $loop->index+1 }}</button>">
                                    <img src="{{ asset($secondBanner->banner_img) }}">
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>

			</div>
		</div>
	</section>
	<!-- A-banner Section End -->





@stop


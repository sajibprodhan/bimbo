@extends('layouts.website')
@section('content')

    <!-- Popular Newspaper links section start -->
    <section class="popular-newspaper-section">
        <div class="container">
            <div class="heading">
                <h2 class="title">
                    <span class="icon"><i class="far fa-newspaper"></i></span>
                    Print & Online Media(BD)
                </h2>
            </div>
            <div class="main-content">
                @if(count($g_print_and_news_all_data) > 0)
                    @foreach($g_print_and_news_all_data as $print_and_news_item)
                        <a target="_blank" href="{{$print_and_news_item->link}}" class="news-item">
                            <img src="{{asset($print_and_news_item->image)}}">
                        </a>
                    @endforeach
                @endif
            </div>
        </div>
    </section>
    <!-- Popular Newspaper links section end -->

@stop

@extends('layouts.website')
@section('content')
	<!-- Contact-us-section -->
	<section class="contact-us-section">
		<div class="container">
			<div class="main-heading">
				<h2 class="main-title">Quick Contact</h2>
			</div>
			<div class="row">
				<div class="col-md-3 offset-md-2 pl-md-5">
					<div class="home-about-us-left-box pl-md-4">
						<div class="home-about-us-left-item">
							<div class="heading">
								<h2 class="title">Address</h2>
							</div>
							<div class="content">
								<ul class="about-us-usefull-links-wrapper">
									<li class="address">
										{!! $g_about_us->address !!}
									</li>
								</ul>
							</div>
						</div>
						<div class="home-about-us-left-item">
							<div class="heading">
								<h2 class="title">Phone Number</h2>
							</div>
							<div class="content">
								<ul class="about-us-usefull-links-wrapper">
									<li class="contact">
										<div><span class="title"><i class="fas fa-phone"></i></span>
										<a class="link" href="tel:{{$g_about_us->phone}}">
											{{$g_about_us->phone}}
                                        </a></div>

										<div><span class="title"><i class="fas fa-phone"></i></span>
										<a class="link" href="tel:{{ $g_about_us->phone2 }}">
											{{ $g_about_us->phone2 }}
                                        </a></div>

										<div><span class="title"><i class="fas fa-phone"></i></span>
										<a class="link" href="tel:{{ $g_about_us->phone3 }}">
											{{ $g_about_us->phone3 }}
                                        </a></div>

									</li>
								</ul>
							</div>
						</div>
						<div class="home-about-us-left-item">
							<div class="heading">
								<h2 class="title">Email</h2>
							</div>
							<div class="content">
								<ul class="about-us-usefull-links-wrapper">
									<li class="contact">

                                        <div><span class="title"><i class="far fa-envelope"></i></span>
                                        <a class="link" href="mailto:{{$g_about_us->email}}">
                                            {{$g_about_us->email}}
                                        </a></div>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-6">
					<div class="contact-us-form-wrapper">
						<div class="contact-us-form-inner">
							<form class="row" action="{{route('contact-us.message')}}" method="post">
								@csrf
								<div class="col-md-6 pr-md-1 input-group">
                                    <label for="name">Name<span> *</span></label>
									<input id="name" type="text" name="name" placeholder="Name" required>
								</div>
								<div class="col-md-6 pl-md-1 input-group">
                                    <label for="phone">Phone<span> *</span></label>
									<input id="phone" type="number" name="phone" placeholder="Phone" required>
								</div>
								<div class="col-md-12 input-group email">
                                    <label for="email">Email<span> *</span></label>
									<input id="email" type="email" name="email" placeholder="Email" required>
								</div>
								<div class="col-md-12 input-group email">
                                    <label for="message">Message<span> *</span></label>
									<input type="text" name="subject" placeholder="Subject" required>
								</div>
								<div class="col-md-12 input-group email">
									<textarea id="message" name="message" placeholder="Message" rows="5"></textarea>
								</div>
								{{-- <!-- Recaptcha start -->
								<div class="g-recaptcha" data-sitekey="6Lec0MwZAAAAAHa-rDGpojrJY3rQjj-BoPA-ngBh"></div>
								<span id="captcha" style="color:red" /></span> <!--Error Message-->
								<!-- Recaptcha end --> --}}
								<br>
								<div class="button-wrapper">
									<button type="submit" name="submit">Send</button>
								</div>
							</form>
							<span class="success-or-error">
							<?php

								if(!empty($_SESSION['success'])){
									echo $_SESSION['success'];
									$success = $_SESSION['success'];
									echo '<script type="text/javascript">swal("Thank You!","'.$success.'", "success")</script>';
								}

								if(!empty($_SESSION['error'])){
									$error = $_SESSION['error'];
									echo '<script type="text/javascript">swal("'.$error.'");</script>';
								}

								if(!empty($_SESSION['validation_error'])){
									$validationError = $_SESSION['validation_error'];
									echo '<script type="text/javascript">swal("'.$validationError.'");</script>';
								}
							?>
							</span>
						</div>
					</div>
                </div>
                <div class="col-md-1"></div>
			</div>
		</div>
	</section>
	<!-- //Contact-us-section -->

	<!-- Map Section Start -->
	<section class="company-location-map-section" style="margin: 0;">
		<div class="company-location-map-wrapper">
			{!! $g_about_us->embeded_link !!}
		</div>
	</section>
	<!-- Map Section End -->
<?php
	unset($_SESSION['validation_error']);
	unset($_SESSION['error']);
	unset($_SESSION['success']);
?>
@endsection

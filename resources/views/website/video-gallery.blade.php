@extends('layouts.website')
@section('content')
	<!-- Image Grid Start -->
	<section class="video-gallery-grid-section">
		<div class="container">
			<div class="main-heading">
				<h2 class="main-title">Video Gallery</h2>
			</div>
            @if(count($g_video_gallery_data) > 0)
			    <div class="row all-video-item-wrapper">

					@foreach($g_video_gallery_data as $video_data)
						<div class="col-md-6">
							<div class="video-item">
						    	{!! $video_data->video_url !!}
						    </div>
						</div>
					@endforeach
                </div>
                <div class="pagination-section">
                    {!! $g_video_gallery_data->links() !!}
                </div>
            @endif
		</div>
	</section>
	<!-- Image Grid End -->
@endsection

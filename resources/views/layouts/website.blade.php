<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	@include('website._partials.stylesheet')
	<style type="text/css">
		/*Loader Css start*/
		.pa-preloader {
		    background-color: #fff;
		    bottom: 0;
		    height: 100%;
		    left: 0;
		    position: fixed;
		    z-index: 99999999999;
		    right: 0;
		    top: 0;
		    width: 100%;
		}
		.pa-ellipsis {
		    margin: 0 auto;
		    position: relative;
		    top: 50%;
		    -moz-transform: translateY(-50%);
		    -webkit-transform: translateY(-50%);
		    transform: translateY(-50%);
		    width: 64px;
		    text-align: center;
		    z-index: 9999;
		}
		.pa-ellipsis span {
		    display: inline-block;
		    width: 15px;
		    height: 15px;
		    border-radius: 50%;
		    background: #04c37f;
		    -webkit-animation: ball-pulse-sync 1s 0s infinite ease-in-out;
		    animation: ball-pulse-sync 1s 0s infinite ease-in-out;
		}
		.pa-ellipsis span:nth-child(1) {
		    -webkit-animation:ball-pulse-sync 1s -.14s infinite ease-in-out;
		    animation:ball-pulse-sync 1s -.14s infinite ease-in-out
		}
		.pa-ellipsis span:nth-child(2) {
		    -webkit-animation:ball-pulse-sync 1s -70ms infinite ease-in-out;
		    animation:ball-pulse-sync 1s -70ms infinite ease-in-out
		}
		@-webkit-keyframes ball-pulse-sync {
		    33% {
		        -webkit-transform:translateY(10px);
		        transform:translateY(10px)
		 }
		    66% {
		        -webkit-transform:translateY(-10px);
		        transform:translateY(-10px)
		    }
		    100% {
		        -webkit-transform:translateY(0);
		        transform:translateY(0)
		    }
		}
		@keyframes ball-pulse-sync {
		    33% {
		        -webkit-transform:translateY(10px);
		        transform:translateY(10px)
		    }
		    66% {
		        -webkit-transform:translateY(-10px);
		        transform:translateY(-10px)
		    }
		    100% {
		        -webkit-transform:translateY(0);
		        transform:translateY(0)
		    }
		}
		/*Loader Css end*/
	</style>
    @yield('styles')
</head>

<body>
    <div class="pa-preloader">
        <div class="pa-ellipsis">
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>
	<a href="#" id="scrollTopBtn" style="display: none;"><span><i class="fas fa-angle-up"></i></span></a>
	@include('website._partials.header')
	<main class="padding-top-190 {{ request()->is('/') ? 'home' : '' }}">
		@yield('content')
	</main>
	@include('website._partials.footer')
	@include('website._partials.scripts')

    @yield('scripts')
</body>
</html>

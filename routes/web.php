<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\Admin\ImportantLinks;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Website\HomeController;
use App\Http\Controllers\Admin\AllImageController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\HeroSliderController;
use App\Http\Controllers\Admin\PostCategoryController;
// use App\Http\Controllers\Subscribe\Auth\ForgotPasswordController;


// Website Routes

// Cache Clear
Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    Artisan::call('config:cache');
    Artisan::call('config:clear');
    Artisan::call('route:clear');
});



Route::get('/', [HomeController::class, 'home'])->name('home');
Route::get('/photo-gallery', [HomeController::class, 'photoGallery'])->name('photo-gallery');
Route::get('/video-gallery', [HomeController::class, 'videoGallery'])->name('video-gallery');
Route::get('/all-news', [HomeController::class, 'allNews'])->name('all-news');


Route::get('/fsingle-footer-detais{id}', [HomeController::class, 'singleFooterTextDetails'])->name('single-footer-detais');
Route::get('/contact-us', [HomeController::class, 'contactUs'])->name('contact-us');
Route::post('/contact-us/message', [HomeController::class, 'contactUsMessage'])->name('contact-us.message');










// Route::group(['prefix' => 'membership', 'as'=>'membership.'], function () {
//     Route::get('/login',  [SubscribeLoginController::class, 'showLoginForm'])->name('login');
//     Route::post('/login',  [SubscribeLoginController::class, 'login' ])->name('login.submit');
//     Route::post('/logout', [SubscribeLoginController::class, 'logout' ])->name('logout');

//     Route::get('/password/reset', [ForgotPasswordController::class, 'showLinkRequestForm'])
//         ->name('password.request');

//     Route::post('/password/email', [ForgotPasswordController::class,'sendResetLinkEmail'])->name('password.email');

//     Route::get('/password/reset/{token}', [ResetPasswordController::class, 'showResetForm'])->name('password.reset');

//     Route::post('/password/reset', [ResetPasswordController::class, 'reset']);

// });


Route::group(['prefix' => '/admin', 'as'=>'admin.'], function () {
	
});


// Admin Panel Routes
Route::group(['middleware' =>'auth'], function () {
	Route::group(['prefix' => '/admin', 'as'=>'admin.'], function () {
		// Admin Module

		Route::resource('/category', PostCategoryController::class);

		Route::match(['get', 'post'],'/settings', [SettingController::class, 'create'])->name('settings');
		// Route::get('/settings', [SettingController::class, 'create'])->name('settings');
		// Route::post('/settings/store', [SettingController::class, 'update'])->name('settings-store');


		Route::get('/profile-edit/{id}', [AdminController::class, 'profileEdit'])->name('profile-edit');
		Route::put('/profile-update/{id}', [AdminController::class, 'profileUpdate'])->name('profile-update');

		// Dashboard Home Module
		Route::get('/', [DashboardController::class, 'dashboard'])->name('dashboard');
	
        Route::resource('/hero-slider', HeroSliderController::class);

 


		// All Image Module
		Route::get('/all-images', [AllImageController::class, 'index'])->name('all-images');
		Route::post('/all-image/store', [AllImageController::class, 'store'])->name('all-image.store');
		Route::delete('/all-image/destroy/{id}', [AllImageController::class, 'destroy'])->name('all-image.destroy');

		
		// Photo & Video Gallery Module
		Route::get('/photo-gallery', [AllImageController::class, 'photoGallery'])->name('photo-gallery');
		Route::post('/photo-gallery/store', [AllImageController::class, 'photoGalleryStore'])->name('photo-gallery.store');
		Route::delete('/photo-gallery/destroy/{id}', [AllImageController::class, 'photoGalleryDestroy'])->name('photo-gallery.destroy');

		Route::get('/video-gallery', [AllImageController::class, 'videoGallery'])->name('video-gallery');
		Route::post('/video-gallery.store', [AllImageController::class, 'videoGalleryStore'])->name('video-gallery.store');
		Route::delete('/video-gallery/destroy/{id}', [AllImageController::class, 'videoGalleryDestroy'])->name('video-gallery.destroy');



        Route::resource('/important-links', ImportantLinks::class);

	});
});

Auth::routes(['register' => false, 'verify' => true]);






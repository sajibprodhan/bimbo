<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function create(){
    	$homeIntroData = HomeIntro::first();
		return view('admin.home-intro.create')->with('home_intro_data', $homeIntroData);
    }

    public function update(Request $request){
    	
    	$homeIntroData = HomeIntro::first();
    	if($homeIntroData){
    		if($request->image){
    			if($homeIntroData->image != ''  && $homeIntroData->image != null){
	               $file_old = base_path().'/'.$homeIntroData->image;
	               //$file_old = public_path().'/'.$imageExistance->image;
	               unlink($file_old);
	            }
    			$coverPhoto = $request->image;
	            $getExt = $coverPhoto->getClientOriginalExtension();
	            $modifiedName = 'img_'.time().'_'.uniqid().'.'.$getExt;
	            $destination ='upload/home-intro-image/';
	            $image = $destination.$modifiedName;
	            $coverPhoto->move( $destination ,$modifiedName );

    			$homeIntroData->update([
	    			'instra_link' => $request->instra_link ,
	    			'fb_link' => $request->fb_link ,
	    			'twitter_link' => $request->twitter_link ,
	    			'youtube_link' => $request->youtube_link ,
	    			'google_link' => $request->google_link ,
	    			'designation' => $request->designation ,
	    		]);
    		}
    	}else{
    		if($request->image){
    			$coverPhoto = $request->image;
	            $getExt = $coverPhoto->getClientOriginalExtension();
	            $modifiedName = 'img_'.time().'_'.uniqid().'.'.$getExt;
	            $destination ='upload/about-us-image/';
	            $image = $destination.$modifiedName;
	            $coverPhoto->move( $destination ,$modifiedName );

    			HomeIntro::create([
	    			'instra_link' => $request->instra_link ,
	    			'fb_link' => $request->fb_link ,
	    			'twitter_link' => $request->twitter_link ,
	    			'youtube_link' => $request->youtube_link ,
	    			'google_link' => $request->google_link ,
	    			'designation' => $request->designation ,
	    		]);
    		}
    	}
    	return back()->with('message', 'Data Updated');
    }
}

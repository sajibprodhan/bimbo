<?php

// namespace App\Http\Controllers\Auth;
namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Models\SubscribeUser;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class SubscribeLoginController extends Controller {

    use AuthenticatesUsers;

    public function __construct(){
        $this->middleware('guest:subscriber', ['except' => ['logout']]);
    }

    public function showLoginForm() {
        return view('subscribe-login.auth.login');
    }

    public function login(Request $request) {
        $this->validate($request, [
            'phone'    => 'required',
            'password' => 'required|min:6'
        ]);

        $subscribeUser = SubscribeUser::where([
            'phone' => $request->phone,
            'status'   => 0
        ])->first();

        if($subscribeUser){
            return redirect()->back()->withErrors('Sorry are not activited!!');
        }else{
            $phone = filter_var($request->phone, FILTER_VALIDATE_EMAIL) ? 'email' : 'phone';
            if (Auth::guard('subscriber')->attempt([$phone => $request->phone, 'password'=> $request->password, 'status'=> true], $request->remember)) {
                return redirect()->intended(route('subscribe-user-profile.index'));
            }else{
                $request->session()->flash('error', 'Your crediantial is miss match');
                return redirect()->back()->withInput($request->only('email', 'remember'));
            }
        }

    }

    public function logout() {
        Auth::guard('subscriber')->logout();
        return redirect('/');
    }

}

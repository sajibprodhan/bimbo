<?php

namespace App\Http\Controllers\Website;

use Validator;
use App\Models\Post;
use App\Mail\ContactMail;
use App\Models\HeroSlider;
use Illuminate\Http\Request;
use App\Models\ImportantLink;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    public function home(){

        // Main Slider Posts
        $homePageData['slider_posts'] = Post::orderBy('id','DESC')->take(5)->get();

        // $homePageData['social_posts'] = Post::where('post_category_id', 'like', '%' . 1 . '%')->orderBy('id','DESC')->take(4)->get();
        // News Posts
        $homePageData['popular_news_posts'] = Post::where('post_category_id', 'like', '%' . 8 . '%')->where('status', 1)->orderBy('id','DESC')->with('PostImage')->take(4)->get();

        $data = array(
            'home_page_data'  => $homePageData,
            'sliders'         => HeroSlider::where('status', 1)->orderBy('id', 'DESC')->get(),
        );

    	return view('website.home')->with($data);
    }




    public function photoGallery(){
    	return view('website.photo-gallery');
    }

    public function videoGallery(){
    	return view('website.video-gallery');
    }

    public function contactUs(){
    	return view('website.contact-us');
    }

    public function contactUsMessage(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'phone' => 'numeric|required',
            'email' => 'email|required',
            'subject' => 'required',
            'message' => 'required',
        ]);

        if ($validator->fails()) {
            $_SESSION['validation_error'] = 'Please Give All Information';
            return back();
        } else {
            $to_email = env('MAIL_FROM_ADDRESS');
            Mail::to($to_email)
                ->queue(new ContactMail($request->all()));
            $_SESSION['success'] = 'Mail Send Successfully';
            return back();

        }
    }


    public function importantLinks($query)
    {
        return ImportantLink::where(['status' => 1, 'related_link' => $query])->take(16)->get();
    }



}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use Illuminate\Support\Str;
use Illuminate\Validation\Rules\Unique;

class PostCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $post_category = Category::orderBy('id', 'DESC')->get();
        return view('admin.post-category.index', compact('post_category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.post-category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

         
        $request->validate([
            'title'       => 'required',
            'trailer'     => 'required',
            'description' => 'required',
            "cover"       => "mimes:jpeg,jpg,png,gif|required",
        ]);

        $data = $request->all();

        $data['slug']  = Str::slug($request->title);

        if( $coverImage = $request->file('cover') ){
            $imageExtension  = $coverImage->getClientOriginalExtension();
            $imagename       = uniqid()."." .$imageExtension;
            $destination     = "upload/category-image/";
            $destinationFull = $destination. $imagename;
            $coverImage->move($destination, $imagename);
            $data['cover'] =  $destinationFull;
        }

        Category::create($data);
 
        return back()->with('message', 'Category Inserted');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categoryData = Category::find($id);
        return view('admin.post-category.edit')->with('category_data', $categoryData);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'category_title' => 'required|unique:post_categories,category_title,' . $id,
        ]);
        Category::find($id)->update($request->all());
        return redirect('admin/post-categories')->with('message', 'Category updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $postData = Category::find($id);
        if ($postData) {
            $postData->delete();
            return redirect('admin/post-categories')->with('message', 'Category Deleted');
        } else {
            return redirect('admin/post-categories')->with('error', 'Data Not Found');
        }

    }
}

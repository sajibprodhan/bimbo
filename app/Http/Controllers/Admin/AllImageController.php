<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\AllImage;
use App\Models\PhotoGallery;
use App\Models\VideoGallery;

class AllImageController extends Controller
{
    public function index(){

    	$allImageData = AllImage::orderBy('id', 'DESC')->get();
    	return view('admin.all-image.index')->with('all_image_data',$allImageData);
    }

    public function store(Request $request){
    	$request->validate([
    		"image_url" => "required|array",
            "image_url.*" => "mimes:jpeg,jpg,png,gif|required",
    	]);
    	if($request->image_url){
            foreach($request->image_url as $key => $image){
                $coverPhoto = $request->image_url[$key];
                $getExt = $coverPhoto->getClientOriginalExtension();
                $modifiedName = 'img_'.time().'_'.uniqid().'.'.$getExt;
                $destination ='upload/all-image/';
                $image_url = $destination.$modifiedName;
                $coverPhoto->move( $destination ,$modifiedName );
                AllImage::create([
                    'image_url' => $image_url,
                ]);
            }
        }
        return back()->with('message','Image Uploaded');
    }

    public function destroy($id){
    	$imageData = AllImage::find($id);
    	if($imageData){
            if($imageData->image_url != ''  && $imageData->image_url != null){
               $file_old = base_path().'/'.$imageData->image_url;
               //$file_old = public_path().'/'.$imageData->image_url;
               unlink($file_old);
            }
    		$imageData->delete();
    		return back()->with('message', 'Image Deleted');
    	}else{
    		return back()->with('error', 'Image Not Found');
    	}
    }


    public function photoGallery(){
        $photoGalleryData = PhotoGallery::get();
        return view('admin.photo-gallery.index')->with('photo_gallery_data', $photoGalleryData);
    }

    public function photoGalleryStore(Request $request){
        $request->validate([
            "title" => "required",
            "image_url" => "required|array",
            "image_url.*" => "mimes:jpeg,jpg,png,gif|required",
        ]);
        if($request->image_url){
            foreach($request->image_url as $key => $image){
                $coverPhoto = $request->image_url[$key];
                $getExt = $coverPhoto->getClientOriginalExtension();
                $modifiedName = 'img_'.time().'_'.uniqid().'.'.$getExt;
                $destination ='upload/photo-gallery/';
                $image_url = $destination.$modifiedName;
                $coverPhoto->move( $destination ,$modifiedName );
                PhotoGallery::create([
                    'title' => $request->title,
                    'image_url' => $image_url,
                ]);
            }
        }
        return back()->with('message','Data Inserted');
    }

    public function photoGalleryDestroy($id){
        $imageData = PhotoGallery::find($id);
        if($imageData){
            if($imageData->image_url != ''  && $imageData->image_url != null){
               $file_old = base_path().'/'.$imageData->image_url;
               //$file_old = public_path().'/'.$imageData->image_url;
               unlink($file_old);
            }
            $imageData->delete();
            return back()->with('message', 'Image Deleted');
        }else{
            return back()->with('error', 'Image Not Found');
        }
    }

    public function videoGallery(){
        $videoGalleryData = VideoGallery::get();
        return view('admin.video-gallery.index')->with('video_gallery_data', $videoGalleryData);
    }

    public function videoGalleryStore(Request $request){
        $request->validate([
            "video_url" => "required",
        ]);
        foreach($request->video_url as $key => $video){
            VideoGallery::create([
                'video_url' => $video,
            ]);
        }
        return back()->with('message','Video Uploaded');
    }

    public function videoGalleryDestroy($id){
        $videoData = VideoGallery::find($id);
        if($videoData){
            $videoData->delete();
            return back()->with('message', 'Image Deleted');
        }else{
            return back()->with('error', 'Image Not Found');
        }
    }
}

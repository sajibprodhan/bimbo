<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\PostCategory;
use App\Models\PostImage;
use App\Models\Post;
use Validator;
use File;

class PostController extends Controller
{
    public function index(){
    	$posts = Post::orderBy('id', 'DESC')->get();
    	return view('admin.posts.index')->with('posts', $posts);
    }

    public function create(){
    	$postCategory = PostCategory::get();
    	return view('admin.posts.create')->with('post_category', $postCategory);
    }

    public function store(Request $request){
    	$request->validate([
    		'title' => 'required',
    		"post_category" => "required|array",
            "post_category.*" => "required",
    		'description' => 'required',
    		'status' => 'required',
    		"post_image" => "required|array",
            "post_image.*" => "mimes:jpeg,jpg,png,gif|required",
    	]);
    	$slug = Str::slug($request->title.'-'.uniqid());
    	$post = Post::create([
    		'title' => $request->title,
    		'slug' => $slug,
    		"post_category_id" => $request->post_category,
    		'description' => $request->description,
    		'status' => $request->status,
    	]);
    	$postId = $post->id;
    	if($request->post_image){
            foreach($request->post_image as $key => $image){
                $coverPhoto = $request->post_image[$key];
                $getExt = $coverPhoto->getClientOriginalExtension();
                $modifiedName = 'img_'.time().'_'.uniqid().'.'.$getExt;
                $destination ='upload/post-image/';
                $post_image = $destination.$modifiedName;
                $coverPhoto->move( $destination ,$modifiedName );
                PostImage::create([
                    'post_id' => $postId,
                    'post_image' => $post_image,
                    'default_image' => $request->default_image[$key],
                ]);
            }
        }
    	return back()->with('message','Post Inserted');
    }


    public function edit($id){
        $postData = Post::where('id', $id)->first();
        if($postData){
            return view('admin.posts.edit')->with('post_data', $postData);
        }else{
            return back()->with('wrror','Post Not Found');
        }
    }

    public function postImageDelete($id){
        $imageExistance = PostImage::find($id);
        if($imageExistance){
            if($imageExistance->post_image != ''  && $imageExistance->post_image != null){
               $old_file = $imageExistance->post_image;
               if(file_exists($old_file)){
                    File::delete($old_file);
                }
            }
            $isDefault = $imageExistance->default_image;
            $postId = $imageExistance->post_id;
            $imageExistance->delete();
            if($isDefault == 1){
                $lastImage = PostImage::where('post_id',$postId)->orderBy('id','DESC')->first();
                if($lastImage){
                    $lastImage->update([
                        'default_image' => 1,
                    ]);
                }
            }
            $status = [
                'success' => 1,
                'is_default' => $isDefault,
            ];
        }else{
            $status = [
                'success' => 0,
                'is_default' => 0,
            ];
        }
        return $status;
    }

    public function update(Request $request, $id){
        $request->validate([
            'title' => 'required',
            "post_category" => "required|array",
            "post_category.*" => "required",
            'description' => 'required',
            'status' => 'required',
            "post_image" => "array",
            "post_image.*" => "mimes:jpeg,jpg,png,gif",
        ]);
        $slug = Str::slug($request->title.'-'.uniqid());
        $post = Post::find($id)->update([
            'title' => $request->title,
            "post_category_id" => $request->post_category,
            'description' => $request->description,
            'status' => $request->status,
        ]);
        $postId = $id;
        if($request->post_image){
            PostImage::where('post_id', $id)->update([
                'default_image' => 0,
            ]);
            foreach($request->post_image as $key => $image){
                $coverPhoto = $request->post_image[$key];
                $getExt = $coverPhoto->getClientOriginalExtension();
                $modifiedName = 'img_'.time().'_'.uniqid().'.'.$getExt;
                $destination ='upload/post-image/';
                $post_image = $destination.$modifiedName;
                $coverPhoto->move( $destination ,$modifiedName );
                PostImage::create([
                    'post_id' => $postId,
                    'post_image' => $post_image,
                    'default_image' => $request->default_image[$key],
                ]);
            }
        }
        return back()->with('message','Post Inserted');
    }

    public function destroy($id){
        $postData = Post::where('id', $id)->first();
        if($postData){
            $postImgData = PostImage::where('post_id', $postData->id);
            if($postImgData){
                $allImgData = $postImgData->get();
                foreach($allImgData as $key => $imgData){
                    if($imgData->post_image != ''  && $imgData->post_image != null){
                       $file_old = base_path().'/'.$imgData->post_image;
                       //$file_old = public_path().'/'.$imgData->post_image;
                       unlink($file_old);
                    }
                }
                $postImgData->delete();
            }
            $postData->delete();
            return back()->with('message','Post Deleted');
        }else{
            return back()->with('error','Post Not Found');
        }
    }
}

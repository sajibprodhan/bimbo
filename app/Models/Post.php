<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Post extends Model
{
    use HasFactory;
    protected $fillable = ['title', 'slug', 'post_category_id', 'description', 'status'];

    protected $casts = [
        'post_category_id' => 'array',
    ];

    public function PostImage(){
        return $this->hasOne(PostImage::class)->orderBy('default_image','DESC');
    }


}

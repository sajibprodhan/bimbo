<?php

namespace App\Providers;
session_start();
use App\Models\AboutUs;
use App\Models\HomeIntro;
use App\Models\PhotoGallery;
use App\Models\PrintAndNews;
use App\Models\VideoGallery;
use App\Models\VisitorCount;
use Illuminate\Support\Carbon;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Session;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Paginator::useBootstrap();

        View::composer(['*'], function($view){

            $photoGalleryData    = PhotoGallery::orderBy('id','desc')->paginate(4);
            $videoGalleryData    = VideoGallery::orderBy('id','desc')->paginate(8);
            $view->with([

                'g_photo_gallery_data'      => $photoGalleryData,
                'g_video_gallery_data'      => $videoGalleryData,
            ]);

        });
    }
}

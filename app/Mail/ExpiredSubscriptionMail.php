<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ExpiredSubscriptionMail extends Mailable
{
    use Queueable, SerializesModels;

    public $mailData;
    public function __construct($mailData)
    {
        $this->mailData = $mailData;
    }

    public function build()
    {
        return $this->subject(str_replace('_',' ', env("APP_NAME")).' Subscription Mail')->markdown('website.mail.expired-subscription-mail')->with('mail_data', $this->mailData);
    }
}

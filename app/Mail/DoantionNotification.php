<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class DoantionNotification extends Mailable
{
    use Queueable, SerializesModels;

    public $amount;
    public function __construct($amount)
    {
        $this->amount = $amount;
    }

    public function build()
    {
        return $this->subject(str_replace('_',' ', env("APP_NAME")).' Donation Nofify')->markdown('website.mail.donation-notification')->with('amount', $this->amount);
    }
}

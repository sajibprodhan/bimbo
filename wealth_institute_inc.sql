-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 16, 2021 at 05:29 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wealth_institute_inc`
--

-- --------------------------------------------------------

--
-- Table structure for table `about_us`
--

CREATE TABLE `about_us` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_pic` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `designation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `embeded_link` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `about_us`
--

INSERT INTO `about_us` (`id`, `name`, `profile_pic`, `profile_description`, `designation`, `phone`, `email`, `address`, `embeded_link`, `created_at`, `updated_at`) VALUES
(1, 'Giash Ahmed', 'upload/about-us-image/img_1609073186_5fe882226956f.jpg', '<p>Giash Ahmed is the Founder and CEO of Wealth Institute Inc. Giash Ahmed holds two decades of accounting and business experience, and has built Wealth Institute Inc. from the ground up since its founding in 2006. Originally from Dhaka, Bangladesh, Giash Ahmed immigrated to New York City in order to further his knowledge of accounting and business at Baruch College. Prior to studying at Baruch, Giash Ahmed earned Bachelor&rsquo;s and Master&rsquo;s degrees in Law from Darul Ihsan University in Dhaka. He is fluent in Bengali, Hindi, Urdu, Punjabi, and English.</p>\r\n\r\n<p>Today, Giash Ahmed maintains his reputation as one of the most trusted and respected accountants in Queens, having established thousands of successful and long term client relationships during his tenure as CEO. Giash Ahmed is an expert in personal and corporate taxation, Federal and State Audits, immigration, accounting, payroll, business consulting, tax planning, and bookkeeping. Giash Ahmed has personally trained all of our employees, and currently resides in Queens with his family.</p>', 'Founder and CEO', '+1-917-744-7308', 'giashahmed123@gmail.com', '<p>37-05, 74th Street, 2nd Floor,</p>\r\n\r\n<p>Jackson Heights, NY 11372</p>', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3022.6021426359953!2d-73.89381308522582!3d40.74877924330266!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c25f071cecf725%3A0xaaacd16fab2c1d07!2s37-05%2074th%20Street%202nd%20floor%2C%20Jackson%20Heights%2C%20NY%2011372%2C%20USA!5e0!3m2!1sen!2sbd!4v1609064579139!5m2!1sen!2sbd\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\" aria-hidden=\"false\" tabindex=\"0\"></iframe>', '2020-12-24 04:06:21', '2020-12-27 06:46:26');

-- --------------------------------------------------------

--
-- Table structure for table `all_images`
--

CREATE TABLE `all_images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `all_images`
--

INSERT INTO `all_images` (`id`, `image_url`, `created_at`, `updated_at`) VALUES
(6, 'upload/all-image/img_1608635687_5fe1d527b3162.jpg', '2020-12-22 05:14:47', '2020-12-22 05:14:47'),
(10, 'upload/all-image/img_1608640379_5fe1e77bb3360.jpg', '2020-12-22 06:32:59', '2020-12-22 06:32:59'),
(11, 'upload/all-image/img_1608640379_5fe1e77bb413c.jpg', '2020-12-22 06:32:59', '2020-12-22 06:32:59'),
(13, 'upload/all-image/img_1608967515_5fe6e55b0b694.png', '2020-12-26 01:25:15', '2020-12-26 01:25:15'),
(14, 'upload/all-image/img_1608967515_5fe6e55b0c026.jpg', '2020-12-26 01:25:15', '2020-12-26 01:25:15'),
(15, 'upload/all-image/img_1608967515_5fe6e55b0cd3d.jpg', '2020-12-26 01:25:15', '2020-12-26 01:25:15'),
(16, 'upload/all-image/img_1608967515_5fe6e55b0d99b.gif', '2020-12-26 01:25:15', '2020-12-26 01:25:15'),
(17, 'upload/all-image/img_1608967515_5fe6e55b0e444.jpg', '2020-12-26 01:25:15', '2020-12-26 01:25:15'),
(18, 'upload/all-image/img_1608967515_5fe6e55b0eb2a.jpg', '2020-12-26 01:25:15', '2020-12-26 01:25:15');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `home_intros`
--

CREATE TABLE `home_intros` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `top_title` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `main_title` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_title` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instra_link` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fb_link` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter_link` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `youtube_link` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google_link` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `home_intros`
--

INSERT INTO `home_intros` (`id`, `image`, `top_title`, `main_title`, `sub_title`, `description`, `instra_link`, `fb_link`, `twitter_link`, `youtube_link`, `google_link`, `created_at`, `updated_at`) VALUES
(1, 'upload/home-intro-image/img_1608979741_5fe7151d93e4a.png', 'Hello I\'m', 'Giash Ahmed', 'Businessman, Politician & Social Activist', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Risus commodo viverra maecenas accumsan lacus vel facilisis.', 'https://www.instagram.com/', 'https://www.facebook.com/giash.ahmed', 'https://www.twitter.com/', 'https://www.youtube.com/', NULL, '2020-12-26 00:35:04', '2020-12-27 02:22:22');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_12_21_093542_create_post_categories_table', 2),
(5, '2020_12_21_093653_create_posts_table', 2),
(6, '2020_12_21_115705_create_post_images_table', 3),
(7, '2020_12_22_093202_create_all_images_table', 4),
(8, '2020_12_24_094646_create_about_us_table', 5),
(9, '2020_12_26_054940_create_home_intros_table', 6),
(10, '2020_12_26_071504_create_photo_galleries_table', 7),
(11, '2020_12_26_071550_create_video_galleries_table', 7),
(12, '2020_12_26_092948_create_print_and_news_table', 8),
(13, '2020_12_28_061139_create_visitor_counts_table', 9);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `photo_galleries`
--

CREATE TABLE `photo_galleries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `photo_galleries`
--

INSERT INTO `photo_galleries` (`id`, `title`, `image_url`, `created_at`, `updated_at`) VALUES
(1, NULL, 'upload/photo-gallery/img_1608967569_5fe6e591397a2.jpg', '2020-12-26 01:26:09', '2020-12-26 01:26:09'),
(2, NULL, 'upload/photo-gallery/img_1608967569_5fe6e5913a940.jpg', '2020-12-26 01:26:09', '2020-12-26 01:26:09'),
(3, NULL, 'upload/photo-gallery/img_1608967569_5fe6e5913b46a.jpg', '2020-12-26 01:26:09', '2020-12-26 01:26:09'),
(4, NULL, 'upload/photo-gallery/img_1608967569_5fe6e5913c346.jpg', '2020-12-26 01:26:09', '2020-12-26 01:26:09'),
(5, NULL, 'upload/photo-gallery/img_1608967569_5fe6e5913c935.gif', '2020-12-26 01:26:09', '2020-12-26 01:26:09'),
(6, NULL, 'upload/photo-gallery/img_1608967569_5fe6e5913d36e.jpg', '2020-12-26 01:26:09', '2020-12-26 01:26:09'),
(7, NULL, 'upload/photo-gallery/img_1608967569_5fe6e5913d971.jpg', '2020-12-26 01:26:09', '2020-12-26 01:26:09'),
(10, 'Lorem Ipsum', 'upload/photo-gallery/img_1609044655_5fe812af2212b.jpg', '2020-12-26 22:50:55', '2020-12-26 22:50:55'),
(12, 'Lorem Ipsum', 'upload/photo-gallery/img_1609044655_5fe812af2436a.jpg', '2020-12-26 22:50:55', '2020-12-26 22:50:55');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `post_category_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0=Unpublished, 1=Published',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `post_category_id`, `title`, `slug`, `description`, `status`, `created_at`, `updated_at`) VALUES
(8, '[\"2\",\"1\",\"8\"]', 'করোনাকালে ডিএসই থেকে রাজস্ব বেড়েছে ৩২ কোটি টাকা', 'kronakale-diesi-theke-rajsw-bereche-32-koti-taka-5fe2e8f0ec8cf', '<p>ঢাকা: ঢাকাসহ সারাদেশে অনুমোদনহীন বেসরকারি স্বাস্থ্য প্রতিষ্ঠানের তথ্য সংগ্রহের কাজ চলছে বলে জানিয়েছেন স্বাস্থ্যমন্ত্রী জাহিদ মালেক। তিনি জানান, প্রয়োজনীয় তথ্য পাওয়া পর অনুমোদনবিহীন প্রতিষ্ঠানগুলোর বিষয়ে যথাযথ আইনি পদক্ষেপ নেওয়া হবে।</p>\r\n\r\n<p><img alt=\"\" src=\"http://localhost:84/Wealth-Institute-Inc-with-Giash-Ahmedlocalhost:84/upload/all-image/img_1608635687_5fe1d527b3162.jpg/upload/all-image/img_1608635687_5fe1d527b3162.jpg\" /></p>\r\n\r\n<h3>Order List</h3>\r\n\r\n<ol>\r\n	<li>List Item</li>\r\n	<li>List Item</li>\r\n</ol>\r\n\r\n<h3>Unorder List</h3>\r\n\r\n<ul>\r\n	<li>gbdfgfdgdf</li>\r\n	<li>dfgsdfgdfgfd</li>\r\n</ul>\r\n\r\n<h2><strong>E</strong></h2>\r\n\r\n<table>\r\n	<tbody>\r\n		<tr>\r\n			<td>E.Coli</td>\r\n			<td>Ear Discharge</td>\r\n			<td>Ear Infections</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Ear Pain</td>\r\n			<td>Eczema</td>\r\n			<td>Elbow Pain</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Emphysema</td>\r\n			<td>Endometriosis</td>\r\n			<td>Enlarged Prostate</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Epididymitis</td>\r\n			<td>Epilepsy</td>\r\n			<td>Epistaxis</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Erectile Dysfunction</td>\r\n			<td>Esophageal Varices</td>\r\n			<td>Eustachian Tube Blockage</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Excessive Sweating</td>\r\n			<td>Eye Floaters</td>\r\n			<td>Eyes DRY</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Eyes Itchy</td>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n	</tbody>\r\n</table>', 1, '2020-12-21 22:54:45', '2020-12-23 05:32:50'),
(13, '[\"7\",\"1\",\"3\",\"8\"]', 'Learn more about side-effect free treatment', 'learn-more-about-side-effect-free-treatment-5fe2e91091f6e', '<p>ঢাকা: ঢাকাসহ সারাদেশে অনুমোদনহীন বেসরকারি স্বাস্থ্য প্রতিষ্ঠানের তথ্য সংগ্রহের কাজ চলছে বলে জানিয়েছেন স্বাস্থ্যমন্ত্রী জাহিদ মালেক। তিনি জানান, প্রয়োজনীয় তথ্য পাওয়া পর অনুমোদনবিহীন প্রতিষ্ঠানগুলোর বিষয়ে যথাযথ আইনি পদক্ষেপ নেওয়া হবে।</p>\r\n\r\n<p><a href=\"http://localhost:84/Wealth-Institute-Inc-with-Giash-Ahmed/upload/post-image/img_1608612885_5fe17c15c0715.jpg\"><img alt=\"\" src=\"http://localhost:84/Wealth-Institute-Inc-with-Giash-Ahmed/upload/post-image/img_1608612885_5fe17c15c0715.jpg\" style=\"height:138px; width:121px\" /></a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3>Order List</h3>\r\n\r\n<ol>\r\n	<li>List Item</li>\r\n	<li>List Item</li>\r\n</ol>\r\n\r\n<h3>Unorder List</h3>\r\n\r\n<ul>\r\n	<li>gbdfgfdgdf</li>\r\n	<li>dfgsdfgdfgfd</li>\r\n</ul>\r\n\r\n<h2><strong>E</strong></h2>\r\n\r\n<table>\r\n	<tbody>\r\n		<tr>\r\n			<td>E.Coli</td>\r\n			<td>Ear Discharge</td>\r\n			<td>Ear Infections</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Ear Pain</td>\r\n			<td>Eczema</td>\r\n			<td>Elbow Pain</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Emphysema</td>\r\n			<td>Endometriosis</td>\r\n			<td>Enlarged Prostate</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Epididymitis</td>\r\n			<td>Epilepsy</td>\r\n			<td>Epistaxis</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Erectile Dysfunction</td>\r\n			<td>Esophageal Varices</td>\r\n			<td>Eustachian Tube Blockage</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Excessive Sweating</td>\r\n			<td>Eye Floaters</td>\r\n			<td>Eyes DRY</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Eyes Itchy</td>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n	</tbody>\r\n</table>', 1, '2020-12-21 22:54:45', '2020-12-23 00:52:00'),
(14, '[\"2\",\"7\",\"3\",\"8\"]', 'Lorem Ipsum', 'lorem-ipsum-5fe2f67de6ad5', '<p>ঢাকা: ঢাকাসহ সারাদেশে অনুমোদনহীন বেসরকারি স্বাস্থ্য প্রতিষ্ঠানের তথ্য সংগ্রহের কাজ চলছে বলে জানিয়েছেন স্বাস্থ্যমন্ত্রী জাহিদ মালেক। তিনি জানান, প্রয়োজনীয় তথ্য পাওয়া পর অনুমোদনবিহীন প্রতিষ্ঠানগুলোর বিষয়ে যথাযথ আইনি পদক্ষেপ নেওয়া হবে।</p>\r\n\r\n<p><img alt=\"\" src=\"https://C:\\xampp\\htdocs\\Wealth-Institute-Inc-with-Giash-Ahmed/upload/all-image/img_1608640379_5fe1e77bb413c.jpg\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3>Order List</h3>\r\n\r\n<ol>\r\n	<li>List Item</li>\r\n	<li>List Item</li>\r\n</ol>\r\n\r\n<h3>Unorder List</h3>\r\n\r\n<ul>\r\n	<li>gbdfgfdgdf</li>\r\n	<li>dfgsdfgdfgfd</li>\r\n</ul>\r\n\r\n<h2><strong>E</strong></h2>\r\n\r\n<table>\r\n	<tbody>\r\n		<tr>\r\n			<td>E.Coli</td>\r\n			<td>Ear Discharge</td>\r\n			<td>Ear Infections</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Ear Pain</td>\r\n			<td>Eczema</td>\r\n			<td>Elbow Pain</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Emphysema</td>\r\n			<td>Endometriosis</td>\r\n			<td>Enlarged Prostate</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Epididymitis</td>\r\n			<td>Epilepsy</td>\r\n			<td>Epistaxis</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Erectile Dysfunction</td>\r\n			<td>Esophageal Varices</td>\r\n			<td>Eustachian Tube Blockage</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Excessive Sweating</td>\r\n			<td>Eye Floaters</td>\r\n			<td>Eyes DRY</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Eyes Itchy</td>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n	</tbody>\r\n</table>', 1, '2020-12-21 22:54:45', '2020-12-23 01:49:17'),
(15, '[\"7\",\"1\",\"3\",\"8\"]', 'Learn more about side-effect free treatment', 'learn-more-about-side-effect-free-treatment-5fe2e92abc494', '<p>ঢাকা: ঢাকাসহ সারাদেশে অনুমোদনহীন বেসরকারি স্বাস্থ্য প্রতিষ্ঠানের তথ্য সংগ্রহের কাজ চলছে বলে জানিয়েছেন স্বাস্থ্যমন্ত্রী জাহিদ মালেক। তিনি জানান, প্রয়োজনীয় তথ্য পাওয়া পর অনুমোদনবিহীন প্রতিষ্ঠানগুলোর বিষয়ে যথাযথ আইনি পদক্ষেপ নেওয়া হবে।</p>\r\n\r\n<p><a href=\"http://localhost:84/Wealth-Institute-Inc-with-Giash-Ahmed/upload/post-image/img_1608612885_5fe17c15c0715.jpg\"><img alt=\"\" src=\"http://localhost:84/Wealth-Institute-Inc-with-Giash-Ahmed/upload/post-image/img_1608612885_5fe17c15c0715.jpg\" style=\"height:138px; width:121px\" /></a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3>Order List</h3>\r\n\r\n<ol>\r\n	<li>List Item</li>\r\n	<li>List Item</li>\r\n</ol>\r\n\r\n<h3>Unorder List</h3>\r\n\r\n<ul>\r\n	<li>gbdfgfdgdf</li>\r\n	<li>dfgsdfgdfgfd</li>\r\n</ul>\r\n\r\n<h2><strong>E</strong></h2>\r\n\r\n<table>\r\n	<tbody>\r\n		<tr>\r\n			<td>E.Coli</td>\r\n			<td>Ear Discharge</td>\r\n			<td>Ear Infections</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Ear Pain</td>\r\n			<td>Eczema</td>\r\n			<td>Elbow Pain</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Emphysema</td>\r\n			<td>Endometriosis</td>\r\n			<td>Enlarged Prostate</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Epididymitis</td>\r\n			<td>Epilepsy</td>\r\n			<td>Epistaxis</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Erectile Dysfunction</td>\r\n			<td>Esophageal Varices</td>\r\n			<td>Eustachian Tube Blockage</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Excessive Sweating</td>\r\n			<td>Eye Floaters</td>\r\n			<td>Eyes DRY</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Eyes Itchy</td>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n	</tbody>\r\n</table>', 1, '2020-12-21 22:54:45', '2020-12-23 00:52:26'),
(16, '[\"2\",\"7\",\"3\",\"8\"]', 'Lorem Ipsum', 'lorem-ipsum-5fe2e9445518f', '<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Recusandae consequatur, blanditiis! Obcaecati quae labore consequatur recusandae, ad velit ex inventore porro modi odio magnam explicabo soluta hic accusamus quibusdam saepe.</p>\r\n\r\n<p><a href=\"http://localhost:84/Wealth-Institute-Inc-with-Giash-Ahmed/upload/post-image/img_1608612885_5fe17c15c0715.jpg\"><img alt=\"\" src=\"http://localhost:84/Wealth-Institute-Inc-with-Giash-Ahmed/upload/post-image/img_1608612885_5fe17c15c0715.jpg\" style=\"height:138px; width:121px\" /></a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3>Order List</h3>\r\n\r\n<ol>\r\n	<li>List Item</li>\r\n	<li>List Item</li>\r\n</ol>\r\n\r\n<h3>Unorder List</h3>\r\n\r\n<ul>\r\n	<li>gbdfgfdgdf</li>\r\n	<li>dfgsdfgdfgfd</li>\r\n</ul>\r\n\r\n<h2><strong>E</strong></h2>\r\n\r\n<table>\r\n	<tbody>\r\n		<tr>\r\n			<td>E.Coli</td>\r\n			<td>Ear Discharge</td>\r\n			<td>Ear Infections</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Ear Pain</td>\r\n			<td>Eczema</td>\r\n			<td>Elbow Pain</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Emphysema</td>\r\n			<td>Endometriosis</td>\r\n			<td>Enlarged Prostate</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Epididymitis</td>\r\n			<td>Epilepsy</td>\r\n			<td>Epistaxis</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Erectile Dysfunction</td>\r\n			<td>Esophageal Varices</td>\r\n			<td>Eustachian Tube Blockage</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Excessive Sweating</td>\r\n			<td>Eye Floaters</td>\r\n			<td>Eyes DRY</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Eyes Itchy</td>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n	</tbody>\r\n</table>', 1, '2020-12-21 22:54:45', '2020-12-23 00:52:52');

-- --------------------------------------------------------

--
-- Table structure for table `post_categories`
--

CREATE TABLE `post_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `post_categories`
--

INSERT INTO `post_categories` (`id`, `category_title`, `created_at`, `updated_at`) VALUES
(1, 'Social Activities', '2020-12-21 04:18:36', '2020-12-22 02:57:57'),
(2, 'Political Activities', '2020-12-21 04:22:01', '2020-12-21 04:22:01'),
(3, 'Business Activities', '2020-12-21 04:22:01', '2020-12-21 04:22:01'),
(7, 'Religious Activities', '2020-12-22 03:46:48', '2020-12-22 03:46:48'),
(8, 'News', '2020-12-22 06:32:47', '2020-12-22 06:32:47');

-- --------------------------------------------------------

--
-- Table structure for table `post_images`
--

CREATE TABLE `post_images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) NOT NULL,
  `post_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `default_image` tinyint(1) NOT NULL DEFAULT 0 COMMENT '1=Default-True, 0=Default-False',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `post_images`
--

INSERT INTO `post_images` (`id`, `post_id`, `post_image`, `default_image`, `created_at`, `updated_at`) VALUES
(18, 13, 'upload/post-image/img_1608706288_5fe2e8f0ee67a.gif', 1, '2020-12-21 22:54:45', '2020-12-22 01:34:55'),
(19, 14, 'upload/post-image/img_1608706288_5fe2e8f0ee67a.gif', 1, '2020-12-21 22:54:45', '2020-12-22 01:34:55'),
(20, 15, 'upload/post-image/img_1608706288_5fe2e8f0ee67a.gif', 1, '2020-12-21 22:54:45', '2020-12-22 01:34:55'),
(21, 16, 'upload/post-image/img_1608706288_5fe2e8f0ee67a.gif', 1, '2020-12-21 22:54:45', '2020-12-22 01:34:55'),
(22, 8, 'upload/post-image/img_1608706288_5fe2e8f0ee67a.gif', 1, '2020-12-23 00:51:28', '2020-12-23 00:51:28');

-- --------------------------------------------------------

--
-- Table structure for table `print_and_news`
--

CREATE TABLE `print_and_news` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `print_and_news`
--

INSERT INTO `print_and_news` (`id`, `image`, `link`, `created_at`, `updated_at`) VALUES
(1, 'upload/print-and-news/img_1608978066_5fe70e921e2a4.jpg', 'http://www.dainikamadershomoy.com/', '2020-12-26 03:58:37', '2020-12-26 04:21:06'),
(6, 'upload/print-and-news/img_1608978289_5fe70f7182fff.jpg', 'http://www.bd-pratidin.com/', '2020-12-26 04:24:49', '2020-12-26 04:24:49'),
(7, 'upload/print-and-news/img_1608978308_5fe70f8407374.jpg', 'http://www.mzamin.com/', '2020-12-26 04:25:08', '2020-12-26 04:25:08'),
(8, 'upload/print-and-news/img_1608978322_5fe70f9233fe9.jpg', 'http://www.bhorerkagoj.net/', '2020-12-26 04:25:22', '2020-12-26 04:25:22'),
(9, 'upload/print-and-news/img_1608978338_5fe70fa23418a.jpg', 'http://www.dailyinqilab.com/', '2020-12-26 04:25:38', '2020-12-26 04:25:38'),
(10, 'upload/print-and-news/img_1608978350_5fe70fae7d6b0.jpg', 'http://www.dailyjanakantha.com/', '2020-12-26 04:25:50', '2020-12-26 04:25:50'),
(11, 'upload/print-and-news/img_1608978361_5fe70fb99ce6d.jpg', 'http://www.mzamin.com/', '2020-12-26 04:26:01', '2020-12-26 04:26:01'),
(12, 'upload/print-and-news/img_1608978374_5fe70fc685593.jpg', 'http://www.dailynayadiganta.com/', '2020-12-26 04:26:14', '2020-12-26 04:26:14'),
(13, 'upload/print-and-news/img_1608978392_5fe70fd878338.jpg', 'http://www.prothom-alo.com/', '2020-12-26 04:26:32', '2020-12-26 04:26:32'),
(14, 'upload/print-and-news/img_1608978408_5fe70fe8920ea.jpg', 'http://www.samakal.com.bd/', '2020-12-26 04:26:48', '2020-12-26 04:26:48'),
(15, 'upload/print-and-news/img_1608978419_5fe70ff3dac4e.jpg', 'http://www.dailysangram.com/', '2020-12-26 04:26:59', '2020-12-26 04:26:59'),
(16, 'upload/print-and-news/img_1608978495_5fe7103fd5a49.jpg', 'http://www.bdnews24.com/bangla/', '2020-12-26 04:28:15', '2020-12-26 04:28:15');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `image`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Giash Ahmed', NULL, 'giashahmed123@gmail.com', '2020-12-21 06:51:04', '$2b$10$RWWJM5mJwvvxdfgz0ms43OB7n67C4bVV1mcIOHG6eFwCWBgDPjTPm', NULL, '2020-12-21 06:51:04', '2020-12-21 06:51:04'),
(2, 'Bijoytech', 'upload/admin-image/img_1608973166_5fe6fb6ee289d.jpg', 'bijoytechadmin@gmail.com', '2020-12-21 06:51:04', '$2y$10$lEHE.jmvrau64RQ4uWUZv.hudJTgejhIHEQ98bds7bjoi79FNUdJm', 'Pc0yihHj1I2ukrVqq1yxQMJhqz9wpse3PVq6HDCNIoYmN86xR9MuBl2bA74i', '2020-12-21 06:51:04', '2020-12-26 02:59:26');

-- --------------------------------------------------------

--
-- Table structure for table `video_galleries`
--

CREATE TABLE `video_galleries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `video_url` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `video_galleries`
--

INSERT INTO `video_galleries` (`id`, `video_url`, `created_at`, `updated_at`) VALUES
(4, '<iframe width=\"100%\" height=\"340\" src=\"https://www.youtube.com/embed/4-UsbL4kkGI\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', '2020-12-26 02:54:03', '2020-12-26 02:54:03'),
(5, '<iframe width=\"100%\" height=\"340\" src=\"https://www.youtube.com/embed/H-GKaBxkR0o\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', '2020-12-26 02:56:48', '2020-12-26 02:56:48'),
(6, '<iframe width=\"100%\" height=\"340\" src=\"https://www.youtube.com/embed/ZSgJSX5rBPs\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', '2020-12-26 02:56:48', '2020-12-26 02:56:48'),
(7, '<iframe width=\"100%\" height=\"340\" src=\"https://www.youtube.com/embed/4-UsbL4kkGI\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', '2020-12-26 02:56:48', '2020-12-26 02:56:48'),
(8, '<iframe width=\"100%\" height=\"340\" src=\"https://www.youtube.com/embed/H-GKaBxkR0o\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', '2020-12-26 02:56:48', '2020-12-26 02:56:48'),
(9, '<iframe width=\"100%\" height=\"340\" src=\"https://www.youtube.com/embed/ZSgJSX5rBPs\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', '2020-12-26 02:57:03', '2020-12-26 02:57:03');

-- --------------------------------------------------------

--
-- Table structure for table `visitor_counts`
--

CREATE TABLE `visitor_counts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ip` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about_us`
--
ALTER TABLE `about_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `all_images`
--
ALTER TABLE `all_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `home_intros`
--
ALTER TABLE `home_intros`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `photo_galleries`
--
ALTER TABLE `photo_galleries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_categories`
--
ALTER TABLE `post_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_images`
--
ALTER TABLE `post_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `print_and_news`
--
ALTER TABLE `print_and_news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `video_galleries`
--
ALTER TABLE `video_galleries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `visitor_counts`
--
ALTER TABLE `visitor_counts`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about_us`
--
ALTER TABLE `about_us`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `all_images`
--
ALTER TABLE `all_images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `home_intros`
--
ALTER TABLE `home_intros`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `photo_galleries`
--
ALTER TABLE `photo_galleries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `post_categories`
--
ALTER TABLE `post_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `post_images`
--
ALTER TABLE `post_images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `print_and_news`
--
ALTER TABLE `print_and_news`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `video_galleries`
--
ALTER TABLE `video_galleries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `visitor_counts`
--
ALTER TABLE `visitor_counts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
